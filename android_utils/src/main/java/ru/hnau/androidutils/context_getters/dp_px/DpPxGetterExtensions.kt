package ru.hnau.androidutils.context_getters.dp_px

import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp


val dp1: DpPxGetter
    get() = dp(1)

val dp2: DpPxGetter
    get() = dp(2)

val dp4: DpPxGetter
    get() = dp(4)

val dp6: DpPxGetter
    get() = dp(6)

val dp8: DpPxGetter
    get() = dp(8)

val dp12: DpPxGetter
    get() = dp(12)

val dp16: DpPxGetter
    get() = dp(16)

val dp24: DpPxGetter
    get() = dp(24)

val dp32: DpPxGetter
    get() = dp(32)

val dp40: DpPxGetter
    get() = dp(40)

val dp48: DpPxGetter
    get() = dp(48)

val dp56: DpPxGetter
    get() = dp(56)

val dp64: DpPxGetter
    get() = dp(64)

val dp72: DpPxGetter
    get() = dp(72)

val dp80: DpPxGetter
    get() = dp(80)

val dp96: DpPxGetter
    get() = dp(96)

val dp112: DpPxGetter
    get() = dp(112)

val dp128: DpPxGetter
    get() = dp(128)

val dp144: DpPxGetter
    get() = dp(144)

val dp160: DpPxGetter
    get() = dp(160)

val dp192: DpPxGetter
    get() = dp(192)

val dp256: DpPxGetter
    get() = dp(256)

val dp320: DpPxGetter
    get() = dp(320)

val dp360: DpPxGetter
    get() = dp(360)

val dp480: DpPxGetter
    get() = dp(480)

val dp512: DpPxGetter
    get() = dp(512)

val dp640: DpPxGetter
    get() = dp(640)

val dp720: DpPxGetter
    get() = dp(720)

val dp960: DpPxGetter
    get() = dp(960)

val dp1024: DpPxGetter
    get() = dp(1024)