package ru.hnau.androidutils.context_getters

import android.content.Context
import ru.hnau.androidutils.ui.utils.ScaleManager
import ru.hnau.androidutils.ui.utils.ThemeManager
import ru.hnau.jutils.getter.mutable.MutableParamGetter
import ru.hnau.jutils.helpers.weak.WeaksContainer


open class ContextGetter<T : Any>(
        dependencies: Iterable<ContextGetterDependency> = emptyList(),
        getter: (Context) -> T
) : MutableParamGetter<Context, T>(
        getter = getter
) {

    companion object {

        private val INSTANCES_WITH_DEPENDENCIES = HashMap<ContextGetterDependency, WeaksContainer<ContextGetter<*>>>()

        init {
            ThemeManager.attach { INSTANCES_WITH_DEPENDENCIES[ContextGetterDependency.THEME]?.reset() }
            ScaleManager.attach { INSTANCES_WITH_DEPENDENCIES[ContextGetterDependency.SCALE]?.reset() }
            //TODO language
        }

        private fun WeaksContainer<ContextGetter<*>>.reset() =
                forEach(ContextGetter<*>::clear)

    }

    init {
        dependencies.forEach { dependency ->
            var dependenciesContainer = INSTANCES_WITH_DEPENDENCIES[dependency]
            if (dependenciesContainer == null) {
                dependenciesContainer = WeaksContainer()
                INSTANCES_WITH_DEPENDENCIES[dependency] = dependenciesContainer
            }
            dependenciesContainer.add(this)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ContextGetter<*>) return false
        val existenceValue = existence
        return existenceValue != null && existenceValue == other.existence
    }

    override fun hashCode() =
            existence?.hashCode() ?: 0

}