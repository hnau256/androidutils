package ru.hnau.androidutils.context_getters.dp_px

import android.content.Context
import android.util.DisplayMetrics
import ru.hnau.androidutils.context_getters.ContextGetter
import ru.hnau.androidutils.ui.utils.ScaleManager
import ru.hnau.jutils.getter.ParamGetter
import ru.hnau.jutils.getter.mutable.MutableGetter
import ru.hnau.jutils.getter.mutable.MutableParamGetter
import ru.hnau.jutils.takeIfPositive


object DpPxUtils {

    private var pxDpFactor = MutableParamGetter<Context, Float> { context ->
        val displayMetrics = context.resources.displayMetrics
        val densityDpi = (displayMetrics.densityDpi.toFloat() * ScaleManager.scaleFactor)
                .takeIfPositive() ?: return@MutableParamGetter 0f

        DisplayMetrics.DENSITY_DEFAULT / densityDpi
    }

    init {
        ScaleManager.attach { pxDpFactor.clear() }
    }

    fun getPxDpFactor(context: Context) = pxDpFactor.get(context)

}


fun pxToDp(context: Context, px: Float) =
        px * DpPxUtils.getPxDpFactor(context)

fun pxToDp(context: Context, px: Int) = pxToDp(context, px.toFloat())
fun pxToDp(context: Context, px: Double) = pxToDp(context, px.toFloat())
fun pxToDpInt(context: Context, px: Float) = pxToDp(context, px).toInt()
fun pxToDpInt(context: Context, px: Int) = pxToDpInt(context, px.toFloat())
fun pxToDpInt(context: Context, px: Double) = pxToDpInt(context, px.toFloat())

fun dpToPx(context: Context, dp: Float): Float {
    val pxDpFactor = DpPxUtils.getPxDpFactor(context).takeIfPositive() ?: return 0f
    return dp / pxDpFactor
}

fun dpToPx(context: Context, dp: Int) = dpToPx(context, dp.toFloat())
fun dpToPx(context: Context, dp: Double) = dpToPx(context, dp.toFloat())
fun dpToPxInt(context: Context, dp: Float) = dpToPx(context, dp).toInt()
fun dpToPxInt(context: Context, dp: Int) = dpToPxInt(context, dp.toFloat())
fun dpToPxInt(context: Context, dp: Double) = dpToPxInt(context, dp.toFloat())