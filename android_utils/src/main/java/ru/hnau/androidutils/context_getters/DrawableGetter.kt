package ru.hnau.androidutils.context_getters

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import ru.hnau.androidutils.ui.drawables.EmptyDrawable
import ru.hnau.androidutils.ui.drawables.HResourcesDrawable
import ru.hnau.androidutils.ui.utils.ThemeManager


class DrawableGetter(
        getter: (context: Context) -> Drawable
) : ContextGetter<Drawable>(
        getter = getter,
        dependencies = listOf(
                ContextGetterDependency.SCALE,
                ContextGetterDependency.THEME
        )
) {

    constructor(existenceDrawable: Drawable) : this({ existenceDrawable })

    @Suppress("DEPRECATION")
    constructor(resId: Int) : this({ HResourcesDrawable(it, resId) })

    companion object {

        val EMPTY = DrawableGetter(EmptyDrawable)

        fun choose(getter: (context: Context) -> DrawableGetter) =
                DrawableGetter { getter.invoke(it).get(it) }

        fun color(color: ColorGetter) =
                DrawableGetter { ColorDrawable(color.get(it)) }


    }


    fun map(converter: (context: Context, drawable: Drawable) -> Drawable) =
            DrawableGetter { context ->
                val drawable = this.get(context)
                converter.invoke(context, drawable)
            }

}

fun Drawable.toGetter() = DrawableGetter(this)