package ru.hnau.androidutils.context_getters

import android.content.Context
import ru.hnau.androidutils.ui.drawer.ExtraSize


class ExtraSizeGetter(
        getter: (context: Context) -> ExtraSize
) : ContextGetter<ExtraSize>(
        getter = getter,
        dependencies = listOf(ContextGetterDependency.LANGUAGE)
) {

    constructor(existenceExtraSize: ExtraSize) : this({ existenceExtraSize })

    companion object {

        val EMPTY = ExtraSizeGetter(ExtraSize.EMPTY)

    }

    operator fun plus(other: ExtraSizeGetter) = ExtraSizeGetter {context ->
        this.get(context) + other.get(context)
    }

    operator fun plus(string: ExtraSize) = plus(string.toGetter())

    fun map(converter: (context: Context, string: ExtraSize) -> ExtraSize) =
            ExtraSizeGetter { context ->
                val string = this.get(context)
                converter.invoke(context, string)
            }

}

fun ExtraSize.toGetter() = ExtraSizeGetter(this)