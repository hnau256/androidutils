package ru.hnau.androidutils.animations


class AnimationHeaviness(
        val value: Float
) {

    companion object {

        val EASY = AnimationHeaviness(0f)
        val MEDIUM = AnimationHeaviness(0.5f)
        val HARD = AnimationHeaviness(1f)

    }

}