package ru.hnau.androidutils.animations

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.detacher.ProducerDetacher


object Animator {

    var maxSupportedHeaviness = AnimationHeaviness.HARD

    fun doAnimation(
            duration: TimeValue,
            onProgress: (Float) -> Unit,
            heaviness: AnimationHeaviness = AnimationHeaviness.MEDIUM,
            onBegin: (() -> Unit)? = null,
            onEnd: (() -> Unit)? = null
    ) {

        onBegin?.invoke()
        onProgress.invoke(0f)

        val durationValue = if (maxSupportedHeaviness.value < heaviness.value) 0L else duration.milliseconds
        if (durationValue <= 0) {
            onProgress.invoke(1f)
            onEnd?.invoke()
            return
        }

        val floatDuration = durationValue.toFloat()
        val start = System.currentTimeMillis()

        var animationMetronomeDetacher: ProducerDetacher<Unit>? = null
        animationMetronomeDetacher = AnimationMetronome.attach {

            val now = System.currentTimeMillis()
            val percentage = (now - start).toFloat() / floatDuration
            onProgress.invoke(percentage.coerceAtMost(1f))

            if (percentage >= 1) {
                onEnd?.invoke()
                animationMetronomeDetacher?.detach()
            }

        }

    }

}