package ru.hnau.androidutils.animations

import ru.hnau.androidutils.utils.runUi
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.tryCatch
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.thread


object AnimationMetronome : Producer<Unit>() {

    private val lock = ReentrantLock()

    var period: TimeValue = TimeValue.MILLISECOND * 10

    val ticTime: Long
        get() = period.milliseconds.coerceAtLeast(1)

    init {

        lock.lock()

        thread(start = true, isDaemon = true) {

            while (true) {

                runUi { call(Unit) }

                tryCatch { Thread.sleep(ticTime) }

                lock.lock()
                lock.unlock()
            }

        }
    }

    override fun onFirstAttached() {
        super.onFirstAttached()
        lock.unlock()
    }

    override fun onLastDetached() {
        super.onLastDetached()
        lock.lock()
    }

}