package ru.hnau.androidutils.coroutines

import android.util.Log
import android.view.View
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import ru.hnau.androidutils.ui.view.utils.ViewIsVisibleToUserProducer
import ru.hnau.jutils.coroutines.launch
import ru.hnau.jutils.getter.mutable.MutableParamGetter
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer
import java.util.*
import kotlin.coroutines.CoroutineContext


class UIJob(isActiveProducer: Producer<Boolean>) {

    private val TAG = UIJob::class.java.simpleName

    private var coroutineContextGetter =
            MutableParamGetter<Job, CoroutineContext> { it + Dispatchers.Main }

    private var job: Job? = null
        set(value) {
            field = value
            coroutineContextGetter.clear()
        }

    private val coroutineContext: CoroutineContext?
        get() = job?.let(coroutineContextGetter::get)

    private val executeQueue = LinkedList<suspend CoroutineScope.() -> Unit>()

    init {
        isActiveProducer.attach {
            it.handle(
                    onTrue = this::start,
                    onFalse = this::stop
            )
        }
    }

    private fun start() = synchronized(this) {
        if (this.job != null) return@synchronized
        val job = SupervisorJob()
        this.job = job
        executeQueue(job)
        Log.d(TAG, "Started")
    }

    private fun stop() = synchronized(this) {
        job?.let { job ->
            job.cancel()
            this.job = null
            Log.d(TAG, "Stopped")
        }
    }

    private fun executeQueue(job: Job) {
        val coroutineContext = coroutineContextGetter.get(job)
        executeQueue.forEach { coroutineContext.launch { it.invoke(this) } }
        executeQueue.clear()
    }

    fun launchIfStarted(action: suspend CoroutineScope.() -> Unit): Boolean {
        val coroutineContext = this.coroutineContext ?: return false
        coroutineContext.launch(action)
        return true
    }

    fun launchAfterStart(action: suspend CoroutineScope.() -> Unit) {
        if (!launchIfStarted(action)) {
            executeQueue.add(action)
        }
    }

}

fun View.createUIJob() = UIJob(ViewIsVisibleToUserProducer(this))