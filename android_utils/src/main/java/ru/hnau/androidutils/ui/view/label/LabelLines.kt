package ru.hnau.androidutils.ui.view.label

import android.graphics.PointF
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import kotlin.math.max
import kotlin.math.min


@Suppress("ConvertToStringTemplate")
class LabelLines(
        private val isRtlGetter: () -> Boolean
) {

    companion object {
        private const val ELLIPSIZE_SYMBOL = '\u2026'
    }

    val size = PointF()

    val offset = PointF()

    val lines = ArrayList<LabelLine>()

    fun update(
            text: String,
            ellipsize: Boolean,
            gravity: HGravity,
            maxWidth: Float?,
            maxHeight: Float?,
            maxLines: Int?,
            lineHeight: Float,
            stringWidthResolver: (String) -> Float
    ) {
        if ((maxWidth ?: 1f) <= 0 || (maxHeight ?: 1f) <= 0 || lineHeight <= 0 || text.isBlank()) {
            lines.clear()
            size.set(0f, 0f)
            return
        }

        val availableLines = if (maxHeight == null) {
            maxLines
        } else {
            min((maxHeight / lineHeight).toInt(), maxLines ?: Int.MAX_VALUE)
        }

        val textFits = separateToLines(text, maxWidth, availableLines, lineHeight, stringWidthResolver)
        if (!textFits && ellipsize) {
            ellipsizeLastLine(maxWidth, stringWidthResolver)
        }

        val maxLineWidth = size.x
        lines.forEachIndexed { i, line ->
            val width = line.width
            line.left = HGravity.calcChildBegin(gravity.horizontalValue, width, 0f, maxLineWidth, isRtlGetter.invoke())
            line.bottom = (i + 1) * lineHeight
        }
    }

    private fun ellipsizeLastLine(
            maxWidth: Float?,
            stringWidthResolver: (String) -> Float
    ) {

        val ellipsizeSymbolWidth = stringWidthResolver.invoke(ELLIPSIZE_SYMBOL.toString())
        if (maxWidth != null && ellipsizeSymbolWidth > maxWidth) {
            return
        }

        lines.mapIndexed { i, line ->
            if (i >= lines.size - 1) {
                var text = line.text

                var textWidth = stringWidthResolver.invoke(text)
                while (maxWidth != null && textWidth + ellipsizeSymbolWidth > maxWidth) {
                    text = text.dropLast(1)
                    textWidth = stringWidthResolver.invoke(text)
                }

                line.text = text + ELLIPSIZE_SYMBOL
                line.width = textWidth + ellipsizeSymbolWidth

                if (line.width > size.x) {
                    size.x = line.width
                }
            }
            line
        }
    }

    private fun separateToLines(
            text: String,
            maxWidth: Float?,
            maxLines: Int?,
            lineHeight: Float,
            stringWidthResolver: (String) -> Float
    ): Boolean {

        lines.clear()

        size.set(0f, 0f)

        val space = " "
        val spaceWidth = stringWidthResolver.invoke(space)

        var fits = true

        var line = ""
        var lineWidth = 0f
        var word = ""
        var wordWidth = 0f

        fun putLineToLinesAndClearLine() {
            lines.add(LabelLine(text = line, width = lineWidth))
            size.x = max(size.x, lineWidth)
            size.y += lineHeight
            line = ""
            lineWidth = 0f
        }

        fun addWordToLineAndClearWord() {
            if (line.isEmpty()) {
                line = word
                lineWidth = wordWidth
            } else {
                line += space + word
                lineWidth += spaceWidth + wordWidth
            }
            word = ""
            wordWidth = 0f
        }

        fun addCharToWord(char: Char, charWidth: Float) {
            word += char
            wordWidth += charWidth
        }

        fun getLineWithWordWidth() =
                if (line.isEmpty()) {
                    wordWidth
                } else {
                    lineWidth + spaceWidth + wordWidth
                }

        text.forEach { char ->
            when (char) {
                ' ' -> {
                    addWordToLineAndClearWord()
                }
                '\n' -> {
                    if (maxLines != null && lines.size >= maxLines) {
                        fits = false
                        return@forEach
                    }
                    addWordToLineAndClearWord()
                    putLineToLinesAndClearLine()
                }
                else -> {
                    val charWidth = stringWidthResolver.invoke(char.toString())
                    if (maxWidth != null && getLineWithWordWidth() + charWidth > maxWidth) {

                        if (maxLines != null && lines.size >= maxLines) {
                            fits = false
                            return@forEach
                        }

                        if (line.isEmpty()) {
                            addWordToLineAndClearWord()
                        }

                        putLineToLinesAndClearLine()

                    }
                    addCharToWord(char, charWidth)
                }
            }
        }

        addWordToLineAndClearWord()

        if (maxLines != null && lines.size >= maxLines && lineWidth > 0) {
            fits = false
        } else {
            putLineToLinesAndClearLine()
        }

        return fits
    }

}