package ru.hnau.androidutils.ui.view.utils

import android.widget.LinearLayout
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


fun LinearLayout.setCenterForegroundGravity() {
    gravity = HGravity.CENTER.getGravity(isRTL)
}

fun LinearLayout.setStartTopForegroundGravity() {
    gravity = HGravity.START_TOP.getGravity(isRTL)
}

fun LinearLayout.setEndTopForegroundGravity() {
    gravity = HGravity.END_TOP.getGravity(isRTL)
}

fun LinearLayout.setEndBottomForegroundGravity() {
    gravity = HGravity.END_BOTTOM.getGravity(isRTL)
}

fun LinearLayout.setStartBottomForegroundGravity() {
    gravity = HGravity.START_BOTTOM.getGravity(isRTL)
}

fun LinearLayout.setTopCenterHorizontalForegroundGravity() {
    gravity = HGravity.TOP_CENTER_HORIZONTAL.getGravity(isRTL)
}

fun LinearLayout.setBottomCenterHorizontalForegroundGravity() {
    gravity = HGravity.BOTTOM_CENTER_HORIZONTAL.getGravity(isRTL)
}

fun LinearLayout.setStartCenterVerticalForegroundGravity() {
    gravity = HGravity.START_CENTER_VERTICAL.getGravity(isRTL)
}

fun LinearLayout.setEndCenterVerticalForegroundGravity() {
    gravity = HGravity.END_CENTER_VERTICAL.getGravity(isRTL)
}