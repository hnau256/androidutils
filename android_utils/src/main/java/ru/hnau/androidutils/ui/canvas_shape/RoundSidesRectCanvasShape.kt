package ru.hnau.androidutils.ui.canvas_shape

import android.graphics.Path
import android.graphics.RectF
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer
import ru.hnau.androidutils.ui.utils.types_utils.initAsRoundSideRect


class RoundSidesRectCanvasShape(
        boundsProducer: BoundsProducer
) : PathCanvasShape(
        boundsProducer
) {

    private val memSaveRect = RectF()

    override fun invalidatePath(bounds: RectF, path: Path) {
        path.initAsRoundSideRect(bounds, memSaveRect)
    }

}