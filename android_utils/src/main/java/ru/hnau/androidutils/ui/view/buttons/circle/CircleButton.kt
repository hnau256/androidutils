package ru.hnau.androidutils.ui.view.buttons.circle

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.PointF
import android.graphics.RectF
import android.view.MotionEvent
import android.view.View
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.bounds_producer.addExtraSize
import ru.hnau.androidutils.ui.canvas_shape.CircleCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.utils.types_utils.initAsCircle
import ru.hnau.androidutils.ui.drawer.shadow.drawer.ButtonShadowDrawer
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import kotlin.math.sqrt


abstract class CircleButton(
        context: Context,
        onClick: () -> Unit,
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT
) : View(context) {

    private val diameter = size.diameter.getPx(context)
    private val radius = diameter / 2f

    private val center = PointF()

    private val boundsProducer = ViewBoundsProducer(this)
            .addExtraSize(shadowInfo.extraSize.get(context))

    private val canvasShape = CircleCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClicked = onClick
    )

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            touchHandler = touchHandler,
            rippleDrawInfo = rippleDrawInfo,
            canvasShape = canvasShape
    )

    private val shadowDrawer = ButtonShadowDrawer(
            animatingView = this,
            touchHandler = touchHandler,
            shadowInfo = shadowInfo,
            canvasShape = canvasShape
    )

    init {
        setSoftwareRendering()
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)

        shadowDrawer.draw(canvas)
        rippleDrawer.draw(canvas)

        drawContent(canvas)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        touchHandler.handle(event)
        return true
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)

        val cx = paddingLeft + shadowDrawer.extraSize.left + (width - horizontalPaddingSum - shadowDrawer.extraSize.left - shadowDrawer.extraSize.right) / 2f
        val cy = paddingTop + shadowDrawer.extraSize.top + (height - verticalPaddingSum - shadowDrawer.extraSize.top - shadowDrawer.extraSize.bottom) / 2f

        center.set(cx, cy)

        layoutContent(
                cx - radius,
                cy - radius,
                cx + radius,
                cy + radius
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = (diameter + shadowDrawer.extraSize.left + shadowDrawer.extraSize.right + horizontalPaddingSum).toInt()
        val height = (diameter + shadowDrawer.extraSize.top + shadowDrawer.extraSize.bottom + verticalPaddingSum).toInt()
        setMeasuredDimension(
                getDefaultMeasurement(widthMeasureSpec, width),
                getDefaultMeasurement(heightMeasureSpec, height)
        )
    }

    protected abstract fun drawContent(canvas: Canvas)

    protected abstract fun layoutContent(left: Float, top: Float, right: Float, bottom: Float)

}