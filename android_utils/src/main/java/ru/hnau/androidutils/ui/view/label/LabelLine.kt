package ru.hnau.androidutils.ui.view.label


data class LabelLine(
        var text: String,
        var width: Float = 0f,
        var left: Float = 0f,
        var bottom: Float = 0f
)