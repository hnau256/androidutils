package ru.hnau.androidutils.ui.view.clickable

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.RelativeLayout
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


@SuppressLint("ViewConstructor")
open class ClickableRelativeLayout(
        context: Context,
        private val onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : RelativeLayout(
        context
) {

    private val boundsProducer =
            createBoundsProducer(false)

    private val canvasShape = RectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClicked = this::onClick
    )

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = rippleDrawInfo
    )

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    protected open fun onClick() {
        onClick?.invoke()
    }

    override fun dispatchDraw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.dispatchDraw(canvas)
    }

}

fun clickableRelativeLayout(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableRelativeLayout.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { ClickableRelativeLayout(it, onClick, rippleDrawInfo) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addClickableRelativeLayout(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableRelativeLayout.() -> Unit)? = null
) =
        addView(
                ClickableRelativeLayout(
                        context,
                        onClick,
                        rippleDrawInfo
                ),
                viewConfigurator
        )