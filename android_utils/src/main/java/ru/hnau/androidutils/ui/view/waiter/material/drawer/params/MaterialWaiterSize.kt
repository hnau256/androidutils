package ru.hnau.androidutils.ui.view.waiter.material.drawer.params

import android.graphics.Paint
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.px
import ru.hnau.androidutils.context_getters.dp_px.dp12
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp24


data class MaterialWaiterSize(
        val radius: DpPxGetter,
        val lineWidthByRadius: Float = 0.2f,
        val lineCap: Paint.Cap = Paint.Cap.BUTT
) {

    val lineWidth = radius * lineWidthByRadius

    val preferredSize = radius * 2 + lineWidth + ADDITIONAL_SIZE_TO_PREFERRED

    companion object {

        private val ADDITIONAL_SIZE_TO_PREFERRED = px(2)

        val SMALL = MaterialWaiterSize(radius = dp12)
        val MEDIUM = MaterialWaiterSize(radius = dp16)
        val LARGE = MaterialWaiterSize(radius = dp24)

        val DEFAULT = LARGE

    }

}