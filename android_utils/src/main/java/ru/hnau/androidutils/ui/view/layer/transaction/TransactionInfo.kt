package ru.hnau.androidutils.ui.view.layer.transaction

import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.animation.Interpolator
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.jutils.TimeValue


data class TransactionInfo(
        val duration: TimeValue = TimeValue.MILLISECOND * 300,
        val emersionSide: Side = Side.END,
        val showInterpolator: Interpolator = LinearOutSlowInInterpolator(),
        val hideInterpolator: Interpolator = FastOutLinearInInterpolator(),
        val shadingColor: ColorGetter = ColorGetter.BLACK.mapWithAlpha(0.75f)
) {

    companion object {

        val DEFAULT = TransactionInfo()

    }

}