package ru.hnau.androidutils.ui.view.header

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setHorizontalPadding
import ru.hnau.androidutils.ui.view.utils.setLinearParams


@SuppressLint("ViewConstructor")
open class HeaderTitle(
        context: Context,
        initialText: StringGetter,
        info: LabelInfo = DEFAULT_INFO,
        horizontalPadding: DpPxGetter = DEFAULT_HORIZONTAL_PADDING
) : Label(
        context,
        initialText,
        info
) {

    constructor(
            context: Context,
            initialText: StringGetter = StringGetter(),
            fontType: FontTypeGetter? = LabelInfo.DEFAULT_FONT_TYPE,
            textColor: ColorGetter = LabelInfo.DEFAULT_TEXT_COLOR,
            textSize: DpPxGetter = DEFAULT_TEXT_SIZE,
            gravity: HGravity = DEFAULT_GRAVITY,
            maxLines: Int? = DEFAULT_MAX_LINES,
            minLines: Int? = DEFAULT_MIN_LINES,
            customLineHeight: DpPxGetter? = LabelInfo.DEFAULT_CUSTOM_LINE_HEIGHT,
            ellipsize: Boolean = LabelInfo.DEFAULT_ELLIPSIZE,
            normalizeForSingleLine: Boolean = LabelInfo.DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
            underline: Boolean = LabelInfo.DEFAULT_UNDERLINE,
            horizontalPadding: DpPxGetter = DEFAULT_HORIZONTAL_PADDING
    ) : this(
            context = context,
            initialText = initialText,
            info = LabelInfo(fontType, textColor, textSize, gravity, maxLines, minLines, customLineHeight, ellipsize, normalizeForSingleLine, underline),
            horizontalPadding = horizontalPadding
    )

    companion object {

        const val DEFAULT_MAX_LINES = 1
        const val DEFAULT_MIN_LINES = 1
        val DEFAULT_TEXT_SIZE = dp(18)
        val DEFAULT_HORIZONTAL_PADDING = dp16
        val DEFAULT_GRAVITY = HGravity.START_CENTER_VERTICAL

        val DEFAULT_INFO = LabelInfo(
                maxLines = DEFAULT_MAX_LINES,
                minLines = DEFAULT_MIN_LINES,
                textSize = DEFAULT_TEXT_SIZE,
                gravity = DEFAULT_GRAVITY
        )

    }

    init {
        setHorizontalPadding(horizontalPadding)
        setLinearParams(0, MATCH_PARENT, 1f)
    }

}

fun headerTitle(
        text: StringGetter,
        info: LabelInfo = HeaderTitle.DEFAULT_INFO,
        horizontalPadding: DpPxGetter = HeaderTitle.DEFAULT_HORIZONTAL_PADDING,
        viewConfigurator: (HeaderTitle.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { HeaderTitle(it, text, info, horizontalPadding) },
        viewConfigurator = viewConfigurator
)

fun headerTitle(
        text: StringGetter = StringGetter(),
        fontType: FontTypeGetter? = LabelInfo.DEFAULT_FONT_TYPE,
        textColor: ColorGetter = LabelInfo.DEFAULT_TEXT_COLOR,
        textSize: DpPxGetter = HeaderTitle.DEFAULT_TEXT_SIZE,
        gravity: HGravity = HeaderTitle.DEFAULT_GRAVITY,
        maxLines: Int? = HeaderTitle.DEFAULT_MAX_LINES,
        minLines: Int? = HeaderTitle.DEFAULT_MIN_LINES,
        customLineHeight: DpPxGetter? = LabelInfo.DEFAULT_CUSTOM_LINE_HEIGHT,
        ellipsize: Boolean = LabelInfo.DEFAULT_ELLIPSIZE,
        normalizeForSingleLine: Boolean = LabelInfo.DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
        underline: Boolean = LabelInfo.DEFAULT_UNDERLINE,
        horizontalPadding: DpPxGetter = HeaderTitle.DEFAULT_HORIZONTAL_PADDING,
        viewConfigurator: (HeaderTitle.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { HeaderTitle(it, text, fontType, textColor, textSize, gravity, maxLines, minLines, customLineHeight, ellipsize, normalizeForSingleLine, underline, horizontalPadding) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addHeaderTitle(
        text: StringGetter,
        info: LabelInfo = HeaderTitle.DEFAULT_INFO,
        horizontalPadding: DpPxGetter = HeaderTitle.DEFAULT_HORIZONTAL_PADDING,
        viewConfigurator: (HeaderTitle.() -> Unit)? = null
) =
        addView(
                HeaderTitle(
                        context,
                        text,
                        info,
                        horizontalPadding
                ),
                viewConfigurator
        )

fun ViewGroup.addHeaderTitle(
        text: StringGetter = StringGetter(),
        fontType: FontTypeGetter? = LabelInfo.DEFAULT_FONT_TYPE,
        textColor: ColorGetter = LabelInfo.DEFAULT_TEXT_COLOR,
        textSize: DpPxGetter = HeaderTitle.DEFAULT_TEXT_SIZE,
        gravity: HGravity = HeaderTitle.DEFAULT_GRAVITY,
        maxLines: Int? = HeaderTitle.DEFAULT_MAX_LINES,
        minLines: Int? = HeaderTitle.DEFAULT_MIN_LINES,
        customLineHeight: DpPxGetter? = LabelInfo.DEFAULT_CUSTOM_LINE_HEIGHT,
        ellipsize: Boolean = LabelInfo.DEFAULT_ELLIPSIZE,
        normalizeForSingleLine: Boolean = LabelInfo.DEFAULT_NORMALIZE_FOR_SINGLE_LINE,
        underline: Boolean = LabelInfo.DEFAULT_UNDERLINE,
        horizontalPadding: DpPxGetter = HeaderTitle.DEFAULT_HORIZONTAL_PADDING,
        viewConfigurator: (HeaderTitle.() -> Unit)? = null
) =
        addView(
                HeaderTitle(
                        context,
                        text,
                        fontType,
                        textColor,
                        textSize,
                        gravity,
                        maxLines,
                        minLines,
                        customLineHeight,
                        ellipsize,
                        normalizeForSingleLine,
                        underline,
                        horizontalPadding
                ),
                viewConfigurator
        )