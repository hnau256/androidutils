package ru.hnau.androidutils.ui.utils.types_utils

import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import ru.hnau.androidutils.ui.utils.ScaleManager


val Drawable.isRTL: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            layoutDirection == View.LAYOUT_DIRECTION_RTL