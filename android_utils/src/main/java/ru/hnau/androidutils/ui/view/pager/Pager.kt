package ru.hnau.androidutils.ui.view.pager

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcelable
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.view.ViewGroup
import android.view.animation.Interpolator
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.view_changer.ViewChanger
import ru.hnau.androidutils.ui.view.view_changer.ViewChangerInfo
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.collections.peekOrNull
import java.util.*


@SuppressLint("ViewConstructor")
class Pager(
        context: Context,
        id: Int,
        private val initialPage: PageInfo<*>,
        private val viewChangerInfo: ViewChangerInfo = ViewChangerInfo(
                viewDecorationDrawer = PagerViewDecorationDrawer(context),
                showInterpolator = PAGE_SHOW_INTERPOLATOR,
                hideInterpolator = PAGE_HIDE_INTERPOLATOR,
                animationTime = TimeValue.MILLISECOND * 400
        )
) : ViewChanger(
        context = context,
        info = viewChangerInfo
), GoBackHandler {

    companion object {

        val DEFAULT_GO_BACK_PAGE_RESOLVER = { pageWrappers: List<PageWrapper> -> (pageWrappers.size - 1).takeIf { it >= 0 } }

        val PAGE_SHOW_INTERPOLATOR = LinearOutSlowInInterpolator()
        val PAGE_HIDE_INTERPOLATOR = Interpolator { it * it }

    }

    private var stack = Stack<PageWrapper>().apply {
        push(PageWrapper(initialPage))
    }
        set(value) {
            field = value
            onStackChanged()
        }

    val pagesInStack: Int
        get() = stack.size

    val canGoBack: Boolean
        get() = pagesInStack > 1

    init {
        this.id = id
        onStackChanged()
    }

    fun showPage(
            showPageInfo: ShowPageInfo,
            precedingPages: List<PageInfoContainer<*>> = emptyList(),
            immediately: Boolean = false
    ) = showPage(
            showPageInfo.pageInfo,
            showPageInfo.clearStack,
            precedingPages,
            immediately
    )

    fun showPage(
            page: PageInfoContainer<*>,
            clearStack: Boolean = false,
            precedingPages: List<PageInfoContainer<*>> = emptyList(),
            immediately: Boolean = false
    ) = synchronized(this) {

        if (clearStack) {
            stack.clear()
        }

        actualizeCurrentPageState(true)

        precedingPages.forEach { stack.push(PageWrapper(it.pageInfo)) }
        val pageWrapper = PageWrapper(page.pageInfo)
        stack.push(pageWrapper)

        showPageAndSetAsCurrent(pageWrapper, true, immediately)
        return@synchronized
    }

    private fun actualizeCurrentPageState(releasePage: Boolean) {
        stack.peekOrNull()?.actualizeState(releasePage)
    }

    fun goBack(
            pageResolver: (pages: List<PageWrapper>) -> Int? = DEFAULT_GO_BACK_PAGE_RESOLVER
    ) = synchronized(this) {
        val wrappers = stack.toList().dropLast(1)
        if (wrappers.isEmpty()) {
            return@synchronized false
        }
        val resolvedWrapperNum = pageResolver.invoke(wrappers)
        if (resolvedWrapperNum == null || resolvedWrapperNum < 0 || resolvedWrapperNum >= wrappers.size) {
            return@synchronized false
        }

        stack.setSize(resolvedWrapperNum + 1)
        val pageWrapper = stack.peek()
        showPageAndSetAsCurrent(pageWrapper, false)

        return@synchronized true
    }

    private fun showPageAndSetAsCurrent(
            pageWrapper: PageWrapper,
            forward: Boolean,
            immediately: Boolean = false
    ) {
        val view = pageWrapper.getPage(this).view
        view.layoutParams = ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT)
        showView(
                view = view,
                fromSide = if (forward) viewChangerInfo.fromSide else viewChangerInfo.fromSide.getOpposite(),
                onTop = forward,
                animationTime = if (immediately) TimeValue.ZERO else getPreferredAnimationTime()
        )
    }

    private fun onStackChanged() {
        val lastPageWrapper = stack.peekOrNull() ?: return
        val page = lastPageWrapper.getPage(this)
        showView(
                view = page.view,
                animationTime = TimeValue.ZERO
        )
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        if (state !is PagerState) {
            super.onRestoreInstanceState(state)
            return
        }
        super.onRestoreInstanceState(state.superState)
        stack = state.stack
    }

    override fun onSaveInstanceState(): PagerState {
        actualizeCurrentPageState(false)
        return PagerState(
                super.onSaveInstanceState(),
                stack
        )
    }

    fun release() = stack.forEach { it.actualizeState(true) }

    override fun handleGoBack() =
            (stack.peekOrNull()?.getPage(this)?.handleGoBack() ?: false) || goBack()

}

fun pager(
        id: Int,
        initialPage: PageInfo<*>,
        viewChangerInfoGetter: (Context) -> ViewChangerInfo = { context ->
            ViewChangerInfo(
                    viewDecorationDrawer = PagerViewDecorationDrawer(context),
                    showInterpolator = Pager.PAGE_SHOW_INTERPOLATOR,
                    hideInterpolator = Pager.PAGE_HIDE_INTERPOLATOR,
                    animationTime = TimeValue.MILLISECOND * 400
            )
        },
        viewConfigurator: (Pager.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { Pager(it, id, initialPage, viewChangerInfoGetter.invoke(it)) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addPager(
        id: Int,
        initialPage: PageInfo<*>,
        viewChangerInfo: ViewChangerInfo = ViewChangerInfo(
                viewDecorationDrawer = PagerViewDecorationDrawer(context),
                showInterpolator = Pager.PAGE_SHOW_INTERPOLATOR,
                hideInterpolator = Pager.PAGE_HIDE_INTERPOLATOR,
                animationTime = TimeValue.MILLISECOND * 400
        ),
        viewConfigurator: (Pager.() -> Unit)? = null
) =
        addView(
                Pager(
                        context,
                        id,
                        initialPage,
                        viewChangerInfo
                ),
                viewConfigurator
        )