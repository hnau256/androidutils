package ru.hnau.androidutils.ui.view.header

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp56
import ru.hnau.androidutils.ui.utils.ScreenManager
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.*


@SuppressLint("ViewConstructor")
open class Header(
        context: Context,
        headerBackgroundColor: ColorGetter = DEFAULT_BACKGROUND_COLOR,
        headerHeight: DpPxGetter = DEFAULT_HEIGHT,
        underStatusBar: Boolean = DEFAULT_UNDER_STATUS_BAR
) : LinearLayout(
        context
) {

    companion object {

        val DEFAULT_BACKGROUND_COLOR = ColorGetter.WHITE
        val DEFAULT_HEIGHT = dp56
        const val DEFAULT_UNDER_STATUS_BAR = true

    }

    private val additionalStatusBarHeight = if (underStatusBar) ScreenManager.statusBarHeight.getPxInt(context) else 0

    private val preferredHeight = headerHeight.getPxInt(context) + additionalStatusBarHeight

    init {
        orientation = HORIZONTAL
        setGravity(HGravity.START_CENTER_VERTICAL)
        setBackgroundColor(headerBackgroundColor)
        setTopPadding(additionalStatusBarHeight)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = getDefaultMeasurement(heightMeasureSpec, preferredHeight)
        val width = getMaxMeasurement(widthMeasureSpec, 0)
        super.onMeasure(
                makeMeasureSpec(width, MeasureSpec.EXACTLY),
                makeMeasureSpec(height, MeasureSpec.EXACTLY)
        )
    }

}

fun header(
        headerBackgroundColor: ColorGetter = Header.DEFAULT_BACKGROUND_COLOR,
        headerHeight: DpPxGetter = Header.DEFAULT_HEIGHT,
        underStatusBar: Boolean = Header.DEFAULT_UNDER_STATUS_BAR,
        viewConfigurator: (Header.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { Header(it, headerBackgroundColor, headerHeight, underStatusBar) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addHeader(
        headerBackgroundColor: ColorGetter = Header.DEFAULT_BACKGROUND_COLOR,
        headerHeight: DpPxGetter = Header.DEFAULT_HEIGHT,
        underStatusBar: Boolean = Header.DEFAULT_UNDER_STATUS_BAR,
        viewConfigurator: (Header.() -> Unit)? = null
) =
        addView(
                Header(context, headerBackgroundColor, headerHeight, underStatusBar),
                viewConfigurator
        )