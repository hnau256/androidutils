package ru.hnau.androidutils.ui.view.clickable

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.LinearLayout
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.bounds_producer.createBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


@SuppressLint("ViewConstructor")
open class ClickableLinearLayout(
        context: Context,
        private val onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : LinearLayout(
        context
) {

    private val boundsProducer =
            createBoundsProducer(false)

    private val canvasShape = RectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClicked = this::onClick
    )

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = rippleDrawInfo
    )

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    protected open fun onClick() {
        onClick?.invoke()
    }

    override fun dispatchDraw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.dispatchDraw(canvas)
    }

}

private fun clickableLinearLayout(
        orientation: Int,
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableLinearLayout.() -> Unit)? = null
) = ViewGetter(
        viewCreator = {
            ClickableLinearLayout(it, onClick, rippleDrawInfo).apply {
                this.orientation = orientation
            }
        },
        viewConfigurator = viewConfigurator
)

fun clickableVerticalLayout(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableLinearLayout.() -> Unit)? = null
) =
        clickableLinearLayout(
                orientation = LinearLayout.VERTICAL,
                onClick = onClick,
                rippleDrawInfo = rippleDrawInfo,
                viewConfigurator = viewConfigurator
        )

fun clickableHorizontalLayout(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableLinearLayout.() -> Unit)? = null
) =
        clickableLinearLayout(
                orientation = LinearLayout.HORIZONTAL,
                onClick = onClick,
                rippleDrawInfo = rippleDrawInfo,
                viewConfigurator = viewConfigurator
        )

private fun ViewGroup.addClickableLinearLayout(
        orientation: Int,
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableLinearLayout.() -> Unit)? = null
) =
        addView(
                ClickableLinearLayout(
                        context,
                        onClick,
                        rippleDrawInfo
                ).apply {
                    this.orientation = orientation
                },
                viewConfigurator
        )

fun ViewGroup.addClickableVerticalLayout(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableLinearLayout.() -> Unit)? = null
) =
        addClickableLinearLayout(
                LinearLayout.VERTICAL,
                onClick,
                rippleDrawInfo,
                viewConfigurator
        )

fun ViewGroup.addClickableHorizontalLayout(
        onClick: (() -> Unit)? = null,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (ClickableLinearLayout.() -> Unit)? = null
) =
        addClickableLinearLayout(
                LinearLayout.HORIZONTAL,
                onClick,
                rippleDrawInfo,
                viewConfigurator
        )