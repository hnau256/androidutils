package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material

import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button.MaterialDialogButtonInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.decoration.MaterialDialogViewDecorationInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.text.MaterialDialogTextInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.title.MaterialDialogTitleInfo


class MaterialDialogViewInfo(
        val paddingHorizontal: DpPxGetter = dp8,
        val paddingVertical: DpPxGetter = dp8,
        val button: MaterialDialogButtonInfo = MaterialDialogButtonInfo.DEFAULT,
        val title: MaterialDialogTitleInfo = MaterialDialogTitleInfo.DEFAULT,
        val text: MaterialDialogTextInfo = MaterialDialogTextInfo.DEFAULT,
        val decoration: MaterialDialogViewDecorationInfo = MaterialDialogViewDecorationInfo.DEFAULT
) {

    companion object {

        val DEFAULT = MaterialDialogViewInfo()

    }

}