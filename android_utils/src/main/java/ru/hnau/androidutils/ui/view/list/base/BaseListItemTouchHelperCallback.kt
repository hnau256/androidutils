package ru.hnau.androidutils.ui.view.list.base

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper


class BaseListItemTouchHelperCallback<T : Any>(
        private val onItemsMoved: (BaseListViewHolder<T>, BaseListViewHolder<T>) -> Boolean,
        private val onItemSwiped: (BaseListViewHolder<T>, SwipeOrDragDirection) -> Unit
) : ItemTouchHelper.Callback() {

    override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
        val viewWrapper = (viewHolder as? BaseListViewHolder<*>)?.viewWrapper ?: return 0
        val dragFlags = viewWrapper.getSupportedDragDirections().combineFlags()
        val swipeFlags = viewWrapper.getSupportedSwipeDirections().combineFlags()
        return ItemTouchHelper.Callback.makeMovementFlags(dragFlags, swipeFlags)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onMove(recyclerView: RecyclerView, source: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        val sourceViewHolder = source as? BaseListViewHolder<T> ?: return false
        val targetViewHolder = target as? BaseListViewHolder<T> ?: return false
        return onItemsMoved.invoke(sourceViewHolder, targetViewHolder)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val baseListViewHolder = viewHolder as? BaseListViewHolder<T> ?: return
        val swipeOrDragDirection = SwipeOrDragDirection.findByFlag(direction) ?: return
        onItemSwiped.invoke(baseListViewHolder, swipeOrDragDirection)
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        super.onSelectedChanged(viewHolder, actionState)

        val baseListViewHolder = viewHolder as? BaseListViewHolder<*> ?: return
        when (actionState) {
            ItemTouchHelper.ACTION_STATE_DRAG -> baseListViewHolder.onSelectedForDragging()
            ItemTouchHelper.ACTION_STATE_SWIPE -> baseListViewHolder.onSelectedForSwiping()
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        (viewHolder as? BaseListViewHolder<*>)?.onUnselected()
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {

        if (!isCurrentlyActive || viewHolder !is BaseListViewHolder<*>) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            return
        }

        val handled = when (actionState) {
            ItemTouchHelper.ACTION_STATE_DRAG -> viewHolder.handleDrawContentViewForDragging(c, dX, dY)
            ItemTouchHelper.ACTION_STATE_SWIPE -> viewHolder.handleDrawContentViewForSwiping(c, dX, dY)
            else -> false
        }

        if (!handled) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
        }


    }


}