package ru.hnau.androidutils.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.setBackgroundColor


@SuppressLint("ViewConstructor")
class LinearLayoutSeparator(
        context: Context,
        weight: Float = 1f,
        width: DpPxGetter = DpPxGetter.ZERO,
        height: DpPxGetter = DpPxGetter.ZERO,
        backgroundColor: ColorGetter? = null
) : View(
        context
) {

    init {

        layoutParams = LinearLayout.LayoutParams(
                width.getPxInt(context),
                height.getPxInt(context),
                weight
        )

        backgroundColor?.let { setBackgroundColor(it) }

    }

}

fun linearSeparator(
        weight: Float = 1f,
        width: DpPxGetter = DpPxGetter.ZERO,
        height: DpPxGetter = DpPxGetter.ZERO,
        backgroundColor: ColorGetter? = null,
        viewConfigurator: (LinearLayoutSeparator.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { LinearLayoutSeparator(it, weight, width, height, backgroundColor) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addLinearSeparator(
        weight: Float = 1f,
        width: DpPxGetter = DpPxGetter.ZERO,
        height: DpPxGetter = DpPxGetter.ZERO,
        backgroundColor: ColorGetter? = null,
        viewConfigurator: (LinearLayoutSeparator.() -> Unit)? = null
) =
        addView(
                LinearLayoutSeparator(
                        context,
                        weight,
                        width,
                        height,
                        backgroundColor
                ),
                viewConfigurator
        )