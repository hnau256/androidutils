package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button

import android.annotation.SuppressLint
import android.content.Context
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.utils.*


@SuppressLint("ViewConstructor")
class MaterialDialogButtons(
        context: Context,
        private val info: MaterialDialogButtonInfo
) : LinearLayout(
        context
) {

    private val preferredHeight = (info.height + info.topMargin).getPxInt(context)

    init {
        orientation = HORIZONTAL
        setEndBottomForegroundGravity()
    }

    fun addButton(
            text: StringGetter,
            onClick: () -> Unit,
            textColor: ColorGetter = info.textColor
    ) {

        val button = MaterialDialogButton(context, text, onClick, info, textColor)
        button.setLinearParams(WRAP_CONTENT, WRAP_CONTENT) {
            val startMargin = if (childCount <= 0) 0 else info.horizontalSeparation.getPxInt(context)
            setRightMargin(if (isRTL) startMargin else 0)
            setLeftMargin(if (isRTL) 0 else startMargin)
        }
        addView(button)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(
                widthMeasureSpec,
                makeExactlyMeasureSpec(getDefaultMeasurement(heightMeasureSpec, preferredHeight))
        )
    }

}