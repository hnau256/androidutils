package ru.hnau.androidutils.ui.view.header.button

import android.content.Context
import android.graphics.Point
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.view.clickable.ClickableView
import ru.hnau.androidutils.ui.view.utils.getMaxSquareMeasuredDimensionSize


abstract class HeaderButton(
        context: Context,
        onClick: () -> Unit,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo()
) : ClickableView(
        context,
        onClick,
        rippleDrawInfo
) {

    private val measuredSize = Point()

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        getMaxSquareMeasuredDimensionSize(widthMeasureSpec, heightMeasureSpec, measuredSize)
        setMeasuredDimension(measuredSize.x, measuredSize.y)
    }

}