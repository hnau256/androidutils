package ru.hnau.androidutils.ui.bounds_producer

import android.content.Context
import android.graphics.RectF
import ru.hnau.androidutils.context_getters.ExtraSizeGetter
import ru.hnau.androidutils.ui.drawer.ExtraSize

fun BoundsProducer.map(
        converter: (fromBounds: RectF, toBounds: RectF) -> Unit
) = object : BoundsProducer() {

    init {
        this@map.attach(this::innerBoundsChanged)
    }

    private fun innerBoundsChanged(innerBounds: RectF) =
            editBounds { converter.invoke(innerBounds, this) }

}

fun BoundsProducer.addExtraSize(
        extraSize: ExtraSize
) = map { fromBounds, toBounds ->
    toBounds.set(
            fromBounds.left + extraSize.left,
            fromBounds.top + extraSize.top,
            fromBounds.right - extraSize.right,
            fromBounds.bottom - extraSize.bottom
    )
}

fun BoundsProducer.addExtraSize(
        context: Context,
        extraSizeGetter: ExtraSizeGetter
) = addExtraSize(
        extraSizeGetter.get(context)
)