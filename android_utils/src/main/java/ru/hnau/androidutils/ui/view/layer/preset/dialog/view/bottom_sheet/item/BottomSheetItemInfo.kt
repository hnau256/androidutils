package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.item


import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp32
import ru.hnau.androidutils.context_getters.dp_px.dp48
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleInfo
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.LabelInfo


data class BottomSheetItemInfo(
        val labelInfo: LabelInfo = LabelInfo(
                fontType = FontTypeGetter.DEFAULT,
                gravity = HGravity.START_CENTER_VERTICAL,
                textSize = dp(20),
                maxLines = null,
                minLines = 1,
                textColor = ColorGetter.BLACK
        ),
        val paddingHorizontal: DpPxGetter = dp32,
        val height: DpPxGetter = dp48,
        val rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(
                rippleInfo = RippleInfo(),
                color = ColorGetter.BLACK,
                backgroundColor = ColorGetter.WHITE,
                rippleAlpha = 0.25f
        )
) {

    companion object {

        val DEFAULT = BottomSheetItemInfo()

    }

}