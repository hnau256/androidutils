package ru.hnau.androidutils.ui.view.header.button

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.setCorrectDrawableLayoutDirection
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum


@SuppressLint("ViewConstructor")
class HeaderIconButton(
        context: Context,
        onClick: () -> Unit,
        icon: DrawableGetter,
        rippleDrawInfo: RippleDrawInfo
) : HeaderButton(
        context,
        onClick,
        rippleDrawInfo
) {

    private val icon = icon.get(context)

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        icon.draw(canvas)
    }

    override fun layout(l: Int, t: Int, r: Int, b: Int) {
        super.layout(l, t, r, b)
        icon.setBounds(paddingLeft, paddingTop, width - horizontalPaddingSum, height - verticalPaddingSum)
        setCorrectDrawableLayoutDirection(icon)
    }

}

fun headerIconButton(
        onClick: () -> Unit,
        icon: DrawableGetter,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (HeaderIconButton.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { HeaderIconButton(it, onClick, icon, rippleDrawInfo) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addHeaderIconButton(
        onClick: () -> Unit,
        icon: DrawableGetter,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        viewConfigurator: (HeaderIconButton.() -> Unit)? = null
) =
        addView(
                HeaderIconButton(
                        context,
                        onClick,
                        icon,
                        rippleDrawInfo
                ),
                viewConfigurator
        )