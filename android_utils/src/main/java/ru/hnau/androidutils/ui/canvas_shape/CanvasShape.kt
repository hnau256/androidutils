package ru.hnau.androidutils.ui.canvas_shape

import android.graphics.*
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer
import ru.hnau.androidutils.ui.utils.types_utils.initAsRoundCornerRect
import ru.hnau.androidutils.ui.utils.types_utils.initAsRoundSideRect
import ru.hnau.jutils.getter.mutable.MutableGetter
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.detacher.ProducerDetachers


abstract class CanvasShape(
        private val boundsProducer: BoundsProducer
) : Producer<Unit>() {

    private var bounds = RectF()
        set(value) {
            field = value
            invalidate(value)
            onChanged()
        }

    private var attachedToBoundsProducer = false

    private fun attachToBoundsProducerIfNeed() {
        synchronized(this) {
            if (attachedToBoundsProducer) {
                return
            }
            attachedToBoundsProducer = true
        }
        boundsProducer.attach { bounds = it }
    }

    fun clip(canvas: Canvas) {
        attachToBoundsProducerIfNeed()
        doClip(canvas)
    }

    fun draw(canvas: Canvas, paint: Paint) {
        attachToBoundsProducerIfNeed()
        doDraw(canvas, paint)
    }

    abstract fun doClip(canvas: Canvas)

    abstract fun doDraw(canvas: Canvas, paint: Paint)

    protected fun onChanged() = call(Unit)

    fun contains(x: Float, y: Float): Boolean {
        attachToBoundsProducerIfNeed()
        return checkContains(x, y) ?: bounds.contains(x, y)
    }

    protected open fun checkContains(x: Float, y: Float): Boolean? = null

    protected abstract fun invalidate(bounds: RectF)

}