package ru.hnau.androidutils.ui.view.getter

import android.support.constraint.ConstraintLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v4.widget.NestedScrollView
import android.view.View
import android.widget.*


fun view(viewConfigurator: (View.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::View,
        viewConfigurator = viewConfigurator
)

fun imageView(viewConfigurator: (ImageView.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::ImageView,
        viewConfigurator = viewConfigurator
)

fun editText(viewConfigurator: (EditText.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::EditText,
        viewConfigurator = viewConfigurator
)

private fun linearLayout(
        orientation: Int,
        viewConfigurator: (LinearLayout.() -> Unit)? = null
) = ViewGetter(
        viewCreator = {
            LinearLayout(it).apply { this.orientation = orientation }
        },
        viewConfigurator = viewConfigurator
)

fun verticalLayout(viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        linearLayout(LinearLayout.VERTICAL, viewConfigurator)

fun horizontalLayout(viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        linearLayout(LinearLayout.HORIZONTAL, viewConfigurator)

fun frameLayout(viewConfigurator: (FrameLayout.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::FrameLayout,
        viewConfigurator = viewConfigurator
)

fun relativeLayout(viewConfigurator: (RelativeLayout.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::RelativeLayout,
        viewConfigurator = viewConfigurator
)

fun scrollView(viewConfigurator: (ScrollView.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::ScrollView,
        viewConfigurator = viewConfigurator
)

fun horizontalScrollView(viewConfigurator: (HorizontalScrollView.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::HorizontalScrollView,
        viewConfigurator = viewConfigurator
)

fun nestedScrollView(viewConfigurator: (NestedScrollView.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::NestedScrollView,
        viewConfigurator = viewConfigurator
)

fun coordinatorLayout(viewConfigurator: (CoordinatorLayout.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::CoordinatorLayout,
        viewConfigurator = viewConfigurator
)

fun constraintLayout(viewConfigurator: (ConstraintLayout.() -> Unit)? = null) = ViewGetter(
        viewCreator = ::ConstraintLayout,
        viewConfigurator = viewConfigurator
)

