package ru.hnau.androidutils.ui.drawer

import kotlin.math.max


data class ExtraSize(
        val left: Float,
        val top: Float = left,
        val right: Float = left,
        val bottom: Float = top
) {

    companion object {

        val EMPTY = ExtraSize(0f)

    }

    constructor(
            left: Int,
            top: Int = left,
            right: Int = left,
            bottom: Int = top
    ) : this(
            left.toFloat(),
            top.toFloat(),
            right.toFloat(),
            bottom.toFloat()
    )

    constructor(
            left: Double,
            top: Double = left,
            right: Double = left,
            bottom: Double = top
    ) : this(
            left.toFloat(),
            top.toFloat(),
            right.toFloat(),
            bottom.toFloat()
    )

    val horizontalSum = left + right

    val verticalSum = top + bottom

    operator fun plus(other: ExtraSize) = ExtraSize(
            left = this.left + other.left,
            top = this.top + other.top,
            right = this.right + other.right,
            bottom = this.bottom + other.bottom
    )

}