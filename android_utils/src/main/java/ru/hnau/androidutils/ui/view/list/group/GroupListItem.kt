package ru.hnau.androidutils.ui.view.list.group


class GroupListItem<G: Any, T: Any> private constructor(
        private val group: G? = null,
        private val item: T? = null
) {

    companion object {

        fun <G: Any, T: Any> createGroup(group: G) = GroupListItem<G, T>(group = group)

        fun <G: Any, T: Any> createItem(item: T) = GroupListItem<G, T>(item = item)

    }

    val isGroup = group != null

    val groupOrItem: Any
        get() = group ?: item!!

}