package ru.hnau.androidutils.ui.utils

import android.app.Activity
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import ru.hnau.androidutils.utils.AutokeyMapCache
import ru.hnau.androidutils.utils.generateId
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.possible.error
import ru.hnau.jutils.possible.success

@Deprecated("Use ru.hnau.androidutils.ui.utils.permissions.PermissionsManager instead")
object PermissionsRequester {

    private val onFinishedsCache = AutokeyMapCache<Int, (Possible<Unit>) -> Unit>(
            keysGenerator = ::generateId,
            tryCount = 1000
    )

    fun requestPermission(
            activity: Activity,
            permissionName: String,
            onNeedShowExplanation: () -> Finisher<Unit>
    ) =
            requestPermissionInner(activity, permissionName, onNeedShowExplanation, true)

    private fun requestPermissionInner(
            activity: Activity,
            permissionName: String,
            onNeedShowExplanation: () -> Finisher<Unit>,
            showExplanation: Boolean
    ): Finisher<Possible<Unit>> = Finisher { onFinished ->

        if (ContextCompat.checkSelfPermission(activity, permissionName) == PackageManager.PERMISSION_GRANTED) {
            onFinished.success(Unit)
            return@Finisher
        }

        if (showExplanation && ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionName)) {
            onNeedShowExplanation.invoke().await {
                requestPermissionInner(activity, permissionName, onNeedShowExplanation, false).await(onFinished)
            }
            return@Finisher
        }

        val requestCode = onFinishedsCache.put(onFinished)
        if (requestCode == null) {
            onFinished.error("")
            return@Finisher
        }

        ActivityCompat.requestPermissions(activity, arrayOf(permissionName), requestCode)

    }


    fun onRequestResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        val onFinished = onFinishedsCache.remove(requestCode) ?: return
        val result = grantResults.getOrNull(0) ?: return

        if (result == PackageManager.PERMISSION_GRANTED) {
            onFinished.success(Unit)
        } else {
            onFinished.error("")
        }

    }


}