package ru.hnau.androidutils.ui.view.utils

import android.os.Build
import android.view.View
import android.view.ViewGroup

private val View.marginLayoutParams: ViewGroup.MarginLayoutParams?
    get() = layoutParams as? ViewGroup.MarginLayoutParams

val View.startMargin: Int
    get() = marginLayoutParams?.let { marginLayoutParams ->
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            marginLayoutParams.marginStart
        } else {
            marginLayoutParams.leftMargin
        }
    } ?: 0

val View.topMargin: Int
    get() = marginLayoutParams?.topMargin ?: 0

val View.endMargin: Int
    get() = marginLayoutParams?.let { marginLayoutParams ->
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            marginLayoutParams.marginEnd
        } else {
            marginLayoutParams.rightMargin
        }
    } ?: 0

val View.bottomMargin: Int
    get() = marginLayoutParams?.bottomMargin ?: 0