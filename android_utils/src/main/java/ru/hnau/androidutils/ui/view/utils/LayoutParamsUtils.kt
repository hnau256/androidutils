package ru.hnau.androidutils.ui.view.utils

import android.support.constraint.ConstraintLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


const val MATCH_PARENT = ViewGroup.LayoutParams.MATCH_PARENT

const val WRAP_CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT

fun <LP : ViewGroup.LayoutParams> View.setLayoutParams(layoutParams: LP, editor: (LP.() -> Unit)? = null) {
    this.layoutParams = editor?.let(layoutParams::apply) ?: layoutParams
}

fun View.setLayoutParams(width: Int, height: Int, editor: (ViewGroup.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(ViewGroup.LayoutParams(width, height), editor)


fun View.setLayoutParams(width: Float, height: Float, editor: (ViewGroup.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(width.toInt(), height.toInt(), editor)

fun View.setLayoutParams(width: DpPxGetter, height: DpPxGetter, editor: (ViewGroup.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(width.getPx(context), height.getPx(context), editor)

fun View.setMarginParams(width: Int, height: Int, editor: (ViewGroup.MarginLayoutParams.() -> Unit)? = null) =
        setLayoutParams(ViewGroup.MarginLayoutParams(width, height), editor)


fun View.setMarginParams(width: Float, height: Float, editor: (ViewGroup.MarginLayoutParams.() -> Unit)? = null) =
        setMarginParams(width.toInt(), height.toInt(), editor)

fun View.setMarginParams(width: DpPxGetter, height: DpPxGetter, editor: (ViewGroup.MarginLayoutParams.() -> Unit)? = null) =
        setMarginParams(width.getPx(context), height.getPx(context), editor)

fun View.setLinearParams(width: Int, height: Int, weight: Float = 0f, editor: (LinearLayout.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(LinearLayout.LayoutParams(width, height, weight), editor)


fun View.setLinearParams(width: Float, height: Float, weight: Float = 0f, editor: (LinearLayout.LayoutParams.() -> Unit)? = null) =
        setLinearParams(width.toInt(), height.toInt(), weight, editor)

fun View.setLinearParams(width: DpPxGetter, height: DpPxGetter, weight: Float = 0f, editor: (LinearLayout.LayoutParams.() -> Unit)? = null) =
        setLinearParams(width.getPx(context), height.getPx(context), weight, editor)

fun View.setFrameParams(width: Int, height: Int, editor: (FrameLayout.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(FrameLayout.LayoutParams(width, height), editor)


fun View.setFrameParams(width: Float, height: Float, editor: (FrameLayout.LayoutParams.() -> Unit)? = null) =
        setFrameParams(width.toInt(), height.toInt(), editor)

fun View.setFrameParams(width: DpPxGetter, height: DpPxGetter, editor: (FrameLayout.LayoutParams.() -> Unit)? = null) =
        setFrameParams(width.getPx(context), height.getPx(context), editor)

fun View.setRecyclerParams(width: Int, height: Int, editor: (RecyclerView.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(RecyclerView.LayoutParams(width, height), editor)


fun View.setRecyclerParams(width: Float, height: Float, editor: (RecyclerView.LayoutParams.() -> Unit)? = null) =
        setRecyclerParams(width.toInt(), height.toInt(), editor)

fun View.setRecyclerParams(width: DpPxGetter, height: DpPxGetter, editor: (RecyclerView.LayoutParams.() -> Unit)? = null) =
        setRecyclerParams(width.getPx(context), height.getPx(context), editor)

fun View.setCoordinatorParams(width: Int, height: Int, behavior: CoordinatorLayout.Behavior<*>? = null, editor: (CoordinatorLayout.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(CoordinatorLayout.LayoutParams(width, height).apply { behavior?.let(this::setBehavior) }, editor)


fun View.setCoordinatorParams(width: Float, height: Float, behavior: CoordinatorLayout.Behavior<*>? = null, editor: (CoordinatorLayout.LayoutParams.() -> Unit)? = null) =
        setCoordinatorParams(width.toInt(), height.toInt(), behavior, editor)

fun View.setCoordinatorParams(width: DpPxGetter, height: DpPxGetter, behavior: CoordinatorLayout.Behavior<*>? = null, editor: (CoordinatorLayout.LayoutParams.() -> Unit)? = null) =
        setCoordinatorParams(width.getPx(context), height.getPx(context), behavior, editor)

fun View.setConstraintParams(width: Int, height: Int, editor: (ConstraintLayout.LayoutParams.() -> Unit)? = null) =
        setLayoutParams(ConstraintLayout.LayoutParams(width, height), editor)


fun View.setConstraintParams(width: Float, height: Float, editor: (ConstraintLayout.LayoutParams.() -> Unit)? = null) =
        setConstraintParams(width.toInt(), height.toInt(), editor)

fun View.setConstraintParams(width: DpPxGetter, height: DpPxGetter, editor: (ConstraintLayout.LayoutParams.() -> Unit)? = null) =
        setConstraintParams(width.getPx(context), height.getPx(context), editor)