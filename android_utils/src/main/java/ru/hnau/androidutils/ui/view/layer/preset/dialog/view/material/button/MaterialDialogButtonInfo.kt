package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp2
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleInfo
import ru.hnau.androidutils.ui.font_type.FontTypeGetter


data class MaterialDialogButtonInfo(
        val textColor: ColorGetter = ColorGetter.BLACK,
        val textFont: FontTypeGetter = FontTypeGetter.DEFAULT_BOLD,
        val textSize: DpPxGetter = dp(14),
        val height: DpPxGetter = dp(36),
        val paddingHorizontal: DpPxGetter = dp8,
        val rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(
                rippleInfo = RippleInfo(),
                color = ColorGetter.BLACK,
                backgroundColor = ColorGetter.WHITE,
                rippleAlpha = 0.25f
        ),
        val borderRadius: DpPxGetter = dp2,
        val horizontalSeparation: DpPxGetter = dp8,
        val topMargin: DpPxGetter = dp(28)
) {

    companion object {

        val DEFAULT = MaterialDialogButtonInfo()

    }

}