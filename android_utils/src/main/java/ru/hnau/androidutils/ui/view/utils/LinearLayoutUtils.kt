package ru.hnau.androidutils.ui.view.utils

import android.widget.LinearLayout
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


fun LinearLayout.setGravity(gravity: HGravity) {
    this.gravity = gravity.getGravity(isRTL)
}

