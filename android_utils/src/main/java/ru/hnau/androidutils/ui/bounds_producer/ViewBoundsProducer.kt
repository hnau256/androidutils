package ru.hnau.androidutils.ui.bounds_producer

import android.graphics.RectF
import android.view.View


class ViewBoundsProducer(
        view: View,
        private val usePaddings: Boolean = true
) : ViewBasedBoundsProducer(
        view = view
) {

    override fun calculateBounds(view: View, bounds: RectF) {

        val paddingLeft = if (usePaddings) view.paddingLeft.toFloat() else 0f
        val paddingTop = if (usePaddings) view.paddingTop.toFloat() else 0f
        val paddingRight = if (usePaddings) view.paddingRight.toFloat() else 0f
        val paddingBottom = if (usePaddings) view.paddingBottom.toFloat() else 0f

        bounds.set(
                paddingLeft,
                paddingTop,
                view.width - paddingRight,
                view.height - paddingBottom
        )

    }

}

fun View.createBoundsProducer(usePaddings: Boolean = true) =
        ViewBoundsProducer(this, usePaddings)