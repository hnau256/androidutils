package ru.hnau.androidutils.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView


@SuppressLint("ViewConstructor")
class LevelsView(
        context: Context,
        innerViews: List<View>
) : FrameLayout(
        context
), GoBackHandler {

    private val innersHandlers = innerViews.mapNotNull { it as? GoBackHandler }.reversed()

    init {
        innerViews.forEach(this::addView)
    }

    override fun handleGoBack() =
            innersHandlers.find(GoBackHandler::handleGoBack) != null

}

fun levelsView(
        innerViews: List<ViewGetter<*>>,
        viewConfigurator: (LevelsView.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { context -> LevelsView(context, innerViews.map { it.get(context) }) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addLevelsView(
        innerViews: List<ViewGetter<*>>,
        viewConfigurator: (LevelsView.() -> Unit)? = null
) =
        addView(
                LevelsView(context, innerViews = innerViews.map { it.get(context) }),
                viewConfigurator
        )