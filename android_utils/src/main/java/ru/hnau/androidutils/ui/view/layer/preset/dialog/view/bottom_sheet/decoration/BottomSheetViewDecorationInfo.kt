package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.decoration

import ru.hnau.androidutils.context_getters.ColorGetter


data class BottomSheetViewDecorationInfo(
        val backgroundColor: ColorGetter = ColorGetter.WHITE
) {

    companion object {

        val DEFAULT = BottomSheetViewDecorationInfo()

    }

}