package ru.hnau.androidutils.ui.canvas_shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer
import kotlin.math.min
import kotlin.math.sqrt


class CircleCanvasShape(
        boundsProducer: BoundsProducer
) : CanvasShape(
        boundsProducer
) {

    private var cx = 0f
    private var cy = 0f
    private var r = 0f
    private val path = Path()

    override fun doClip(canvas: Canvas) {
        canvas.clipPath(path)
    }

    override fun doDraw(canvas: Canvas, paint: Paint) {
        canvas.drawCircle(cx, cy, r, paint)
    }

    override fun invalidate(bounds: RectF) {
        cx = bounds.centerX()
        cy = bounds.centerY()
        r = min(bounds.width(), bounds.height()) / 2
        path.reset()
        path.addCircle(cx, cy, r, Path.Direction.CW)
    }

    override fun checkContains(x: Float, y: Float) =
            sqrt((x - cx) * (x - cx) + (y - cy) * (y - cy)) <= r

}