package ru.hnau.androidutils.ui.canvas_shape

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer
import ru.hnau.androidutils.ui.utils.types_utils.initAsRoundCornerRect


class RoundCornersRectCanvasShape(
        context: Context,
        boundsProducer: BoundsProducer,
        cornersRadius: DpPxGetter
) : PathCanvasShape(
        boundsProducer
) {

    private val cornersRadius = cornersRadius.getPx(context)
    private val memSaveRect = RectF()

    override fun invalidatePath(bounds: RectF, path: Path) {
        path.initAsRoundCornerRect(bounds, cornersRadius, memSaveRect)
    }

}