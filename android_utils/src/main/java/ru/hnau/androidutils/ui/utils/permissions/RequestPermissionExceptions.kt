package ru.hnau.androidutils.ui.utils.permissions

import android.content.Context
import java.lang.RuntimeException
import android.support.v4.content.ContextCompat.startActivity
import android.net.Uri.fromParts
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.support.v4.view.accessibility.AccessibilityEventCompat.setAction
import android.content.Intent
import android.net.Uri
import android.provider.Settings


open class RequestPermissionException(
        message: String,
        private val permissionName: String
) : RuntimeException(
        message
)

class OnPermissionDeniedException(
        permissionName: String
) : RequestPermissionException(
        message = "Permission '$permissionName' denied",
        permissionName = permissionName
)

class OnPermissionDeniedForeverException(
        permissionName: String
) : RequestPermissionException(
        message = "Permission '$permissionName' denied forever",
        permissionName = permissionName
) {

    fun openSettingsToGivePermission(context: Context) {
        context.startActivity(
                Intent().apply {
                    action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    data = Uri.fromParts(
                            "package",
                            context.getPackageName(),
                            null
                    )
                }
        )
    }

}