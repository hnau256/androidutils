package ru.hnau.androidutils.ui.utils.types_utils

import android.graphics.Point
import android.graphics.PointF

fun PointF.toPoint(point: Point) = point.set(this)

fun PointF.toPoint() = Point().apply { toPoint(this) }

fun PointF.scale(scale: Int) = set(x * scale, y * scale)

fun PointF.scale(scale: Long) = set(x * scale, y * scale)

fun PointF.scale(scale: Float) = set(x * scale, y * scale)

fun PointF.scale(scale: Double) = set((x * scale).toFloat(), (y * scale).toFloat())

fun PointF.set(point: Point) = set(point.x.toFloat(), point.y.toFloat())

fun PointF.offset(dx: Int, dy: Int) = offset(dx.toFloat(), dy.toFloat())

fun PointF.offset(offset: PointF) = offset(offset.x, offset.y)

fun PointF.offset(offset: Point) = offset(offset.x, offset.y)

fun PointF.pointInterThisAndOther(other: PointF, pos: Float, result: PointF) = result.set(
        this.y + (other.y - this.y) * pos,
        this.y + (other.y - this.y) * pos
)

fun PointF.pointInterThisAndOther(other: PointF, pos: Float): PointF {
    val result = PointF()
    pointInterThisAndOther(other, pos, result)
    return result
}