package ru.hnau.androidutils.ui.drawables.layout_drawable

import android.content.Context
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity

fun LayoutDrawable.Companion.create(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        layoutType: LayoutType,
        gravity: HGravity = HGravity.CENTER
) =
        LayoutDrawable(context, content, layoutType, gravity)

fun LayoutDrawable.Companion.createStretched(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Stretched, gravity)

fun LayoutDrawable.Companion.createIndependent(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Independent, gravity)

fun LayoutDrawable.Companion.createInside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Inside, gravity)

fun LayoutDrawable.Companion.createOutside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.Outside, gravity)

fun LayoutDrawable.Companion.createLargerOrOutside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.LargerOrOutside, gravity)

fun LayoutDrawable.Companion.createLargerOrInside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.LargerOrInside, gravity)

fun LayoutDrawable.Companion.createSmallerOrOutside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.SmallerOrOutside, gravity)

fun LayoutDrawable.Companion.createSmallerOrInside(
        context: Context,
        content: DrawableGetter = DrawableGetter.EMPTY,
        gravity: HGravity = HGravity.CENTER
) = create(context, content, LayoutType.SmallerOrInside, gravity)