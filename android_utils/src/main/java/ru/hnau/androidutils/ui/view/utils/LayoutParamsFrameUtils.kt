package ru.hnau.androidutils.ui.view.utils

import android.view.Gravity
import android.widget.FrameLayout


fun FrameLayout.LayoutParams.setCenterGravity() {
    gravity = Gravity.CENTER
}

fun FrameLayout.LayoutParams.setStartTopGravity() {
    gravity = Gravity.START or Gravity.TOP
}

fun FrameLayout.LayoutParams.setEndTopGravity() {
    gravity = Gravity.END or Gravity.TOP
}

fun FrameLayout.LayoutParams.setEndBottomGravity() {
    gravity = Gravity.END or Gravity.BOTTOM
}

fun FrameLayout.LayoutParams.setStartBottomGravity() {
    gravity = Gravity.START or Gravity.BOTTOM
}

fun FrameLayout.LayoutParams.setTopCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
}

fun FrameLayout.LayoutParams.setBottomCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM
}

fun FrameLayout.LayoutParams.setStartCenterVerticalGravity() {
    gravity = Gravity.START or Gravity.CENTER_VERTICAL
}

fun FrameLayout.LayoutParams.setEndCenterVerticalGravity() {
    gravity = Gravity.END or Gravity.CENTER_VERTICAL
}