package ru.hnau.androidutils.ui.view.utils

import android.graphics.drawable.Drawable
import android.os.Build
import android.view.View
import android.view.View.LAYOUT_DIRECTION_RTL
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.dpToPx
import ru.hnau.androidutils.context_getters.dp_px.dpToPxInt
import ru.hnau.androidutils.context_getters.dp_px.pxToDp
import ru.hnau.androidutils.context_getters.dp_px.pxToDpInt
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.producer.Producer

fun View.setSoftwareRendering() = setLayerType(View.LAYER_TYPE_SOFTWARE, null)

fun View.postDelayed(pause: TimeValue, action: () -> Unit) =
        postDelayed(action, pause.milliseconds)

fun View.setBackgroundColor(color: ColorGetter) =
        setBackgroundColor(color.get(context))

val View.isRTL: Boolean
    get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 &&
            layoutDirection == LAYOUT_DIRECTION_RTL

fun View.setCorrectDrawableLayoutDirection(drawable: Drawable) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        drawable.layoutDirection = layoutDirection
    }
}

fun <T> View.observeWhenVisibleToUser(producer: Producer<T>, handler: (T) -> Unit) =
        addIsVisibleToUserHandler { isVisibleToUser ->
            if (isVisibleToUser) {
                producer.attach(handler)
            } else {
                producer.detach(handler)
            }
        }

fun View.addIsVisibleToUserHandler(handler: (isVisibleToUser: Boolean) -> Unit) {
    ViewIsVisibleToUserProducer(this).attach(handler)
}

fun View.addShownToUserHandler(handler: () -> Unit) =
        addIsVisibleToUserHandler { it.ifTrue(handler) }

fun View.addHiddenFromUserHandler(handler: () -> Unit) =
        addIsVisibleToUserHandler { it.ifFalse(handler) }

fun <T> View.invalidate(param: T) = invalidate()

fun <T> View.requestLayout(param: T) = requestLayout()

fun View.dpToPx(dp: Int) = dpToPx(context, dp)
fun View.dpToPx(dp: Float) = dpToPx(context, dp)
fun View.dpToPx(dp: Double) = dpToPx(context, dp)
fun View.dpToPxInt(dp: Int) = dpToPxInt(context, dp)
fun View.dpToPxInt(dp: Float) = dpToPxInt(context, dp)
fun View.dpToPxInt(dp: Double) = dpToPxInt(context, dp)

fun View.pxToDp(px: Int) = pxToDp(context, px)
fun View.pxToDp(px: Float) = pxToDp(context, px)
fun View.pxToDp(px: Double) = pxToDp(context, px)
fun View.pxToDpInt(px: Int) = pxToDpInt(context, px)
fun View.pxToDpInt(px: Float) = pxToDpInt(context, px)
fun View.pxToDpInt(px: Double) = pxToDpInt(context, px)

fun View.setVisible() = setVisibility(View.VISIBLE)
fun View.setInvisible() = setVisibility(View.INVISIBLE)
fun View.setGone() = setVisibility(View.GONE)

fun View.setVisibleOrInvisible(visible: Boolean) =
        if (visible) setVisible() else setInvisible()

fun View.setVisibleOrGone(visible: Boolean) =
        if (visible) setVisible() else setGone()
