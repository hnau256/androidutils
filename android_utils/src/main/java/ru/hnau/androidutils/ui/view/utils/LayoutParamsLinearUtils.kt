package ru.hnau.androidutils.ui.view.utils

import android.view.Gravity
import android.widget.LinearLayout


fun LinearLayout.LayoutParams.setCenterGravity() {
    gravity = Gravity.CENTER
}

fun LinearLayout.LayoutParams.setStartTopGravity() {
    gravity = Gravity.START or Gravity.TOP
}

fun LinearLayout.LayoutParams.setEndTopGravity() {
    gravity = Gravity.END or Gravity.TOP
}

fun LinearLayout.LayoutParams.setEndBottomGravity() {
    gravity = Gravity.END or Gravity.BOTTOM
}

fun LinearLayout.LayoutParams.setStartBottomGravity() {
    gravity = Gravity.START or Gravity.BOTTOM
}

fun LinearLayout.LayoutParams.setTopCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
}

fun LinearLayout.LayoutParams.setBottomCenterHorizontalGravity() {
    gravity = Gravity.CENTER_HORIZONTAL or Gravity.BOTTOM
}

fun LinearLayout.LayoutParams.setStartCenterVerticalGravity() {
    gravity = Gravity.START or Gravity.CENTER_VERTICAL
}

fun LinearLayout.LayoutParams.setEndCenterVerticalGravity() {
    gravity = Gravity.END or Gravity.CENTER_VERTICAL
}