package ru.hnau.androidutils.ui.view.layer.layer

import android.content.Context
import ru.hnau.androidutils.go_back_handler.GoBackHandler
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.manager.LayerViewContainer
import ru.hnau.androidutils.ui.view.layer.transaction.TransactionInfo
import ru.hnau.jutils.getter.Getter


class LayerWrapper(
        private val context: Context,
        layer: Layer,
        private val layerViewContainer: LayerViewContainer,
        layerManagerConnector: LayerManagerConnector,
        val transactionInfo: TransactionInfo
) : GoBackHandler {

    private val layerClass = layer.javaClass

    var transparent = layer.transparent

    private val constructor = Getter {
        layerClass.constructors.find { constructor ->
            val parameters = constructor.parameterTypes
            parameters.size == 1 && parameters[0] == Context::class.java
        }
                ?: throw RuntimeException("Unable instantiate ${layerClass.name}, there is no constructor(Context)")
    }

    private val state = LayerStateContainer(layerManagerConnector)

    private var layer: Layer? = layer

    val existenceLayer: Layer?
        get() = layer

    init {
        state.writeLayerManagerConnector(layer)
        layer.afterCreate()
        layerViewContainer.addLayer(layer)
    }

    fun get(): Layer = synchronized(this) {
        var layer = this.layer
        if (layer == null) {
            layer = createNewLayerInstance()
            this.layer = layer
        }
        return layer
    }

    fun release() {
        synchronized(this) {
            val layer = this.layer
            this.layer = null
            layer
        }?.let { layer ->
            layer.beforeDestroy()
            state.readState(layer)
            layerViewContainer.removeLayer(layer)
        }
    }

    private fun createNewLayerInstance(): Layer {
        val layer = constructor.get().newInstance(context) as Layer
        state.writeState(layer)
        state.writeLayerManagerConnector(layer)
        layer.afterCreate()
        layerViewContainer.addLayer(layer)
        return layer
    }

    fun invalidateLayerIfExists() = synchronized(this) {
        if (layer == null) {
            return@synchronized
        }
        release()
        get()
    }

    override fun handleGoBack() =
            layer?.handleGoBack() ?: false

}