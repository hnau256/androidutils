package ru.hnau.androidutils.ui.view.layer.layer

import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.jutils.getter.ParamGetter
import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.util.*
import kotlin.reflect.KClass


class LayerStateContainer(
        private val layerManagerConnector: LayerManagerConnector
) {

    companion object {

        private fun getFields(fields: List<Field>, annotationClass: KClass<*>) =
                fields.filter { field ->
                    val suitable = field.annotations.any { annotation ->
                        annotation.annotationClass == annotationClass
                    }
                    if (suitable) {
                        field.isAccessible = true
                    }
                    return@filter suitable
                }

    }

    private class LayerFields(
            val state: List<Field>,
            val managerConnector: List<Field>
    )

    private val fields = ParamGetter<Layer, LayerFields> { layer ->
        val fields = LinkedList<Field>()
        getNotStaticFields(layer.javaClass, fields)
        LayerFields(
                state = getFields(fields, LayerState::class),
                managerConnector = getFields(fields, ManagerConnector::class)
        )
    }

    private tailrec fun getNotStaticFields(clazz: Class<*>, result: MutableList<Field>) {
        result += clazz.declaredFields.filter { it != null && !Modifier.isStatic(it.modifiers) }
        val superclass = clazz.superclass ?: return
        getNotStaticFields(superclass, result)
    }

    private val state = LinkedHashMap<String, Any>()

    fun readState(layer: Layer) {
        state.clear()
        fields.get(layer)
                .state
                .asSequence()
                .forEach { field ->
                    val value = field.get(layer) ?: return@forEach
                    val name: String = field.name
                    state[name] = value
                }
    }

    fun writeState(layer: Layer) {
        fields.get(layer)
                .state
                .asSequence()
                .forEach { field ->
                    val value = state[field.name] ?: return@forEach
                    field.set(layer, value)
                }
    }

    fun writeLayerManagerConnector(layer: Layer) {
        fields.get(layer)
                .managerConnector
                .asSequence()
                .forEach { field ->
                    field.set(layer, layerManagerConnector)
                }
    }

}