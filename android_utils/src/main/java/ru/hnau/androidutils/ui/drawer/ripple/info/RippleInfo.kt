package ru.hnau.androidutils.ui.drawer.ripple.info

import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.jutils.TimeValue


data class RippleInfo(
        val maxBackgroundShadowOpacity: Float = DEFAULT_MAX_BACKGROUND_SHADOW_OPACITY,
        val minCircleRadius: DpPxGetter = DEFAULT_MIN_CIRCLE_RADIUS,
        val maxCircleRadius: DpPxGetter = DEFAULT_MAX_CIRCLE_RADIUS,
        val minCircleEmergenceTime: TimeValue = DEFAULT_MIN_CIRCLE_EMERGENCE_TIME,
        val minToMaxCircleTransformationTime: TimeValue = DEFAULT_MIN_TO_MAX_CIRCLE_TRANSFORMATION_TIME,
        val backgroundShadowEmergenceTime: TimeValue = DEFAULT_BACKGROUND_SHADOW_EMERGENCE_TIME,
        val backgroundShadowConcealmentTime: TimeValue = DEFAULT_BACKGROUND_SHADOW_CONCEALMENT_TIME,
        val minRadiusInterpolator: Interpolator = DEFAULT_MIN_RADIUS_INTERPOLATOR,
        val minToMaxRadiusInterpolator: Interpolator = DEFAULT_MIN_TO_MAX_RADIUS_INTERPOLATOR
) {
    
    companion object {

        const val DEFAULT_MAX_BACKGROUND_SHADOW_OPACITY = 0.65f
        val DEFAULT_MIN_CIRCLE_RADIUS = dp(32)
        val DEFAULT_MAX_CIRCLE_RADIUS = dp(200)
        val DEFAULT_MIN_CIRCLE_EMERGENCE_TIME = TimeValue.MILLISECOND * 300
        val DEFAULT_MIN_TO_MAX_CIRCLE_TRANSFORMATION_TIME = TimeValue.MILLISECOND * 500
        val DEFAULT_BACKGROUND_SHADOW_EMERGENCE_TIME = TimeValue.MILLISECOND * 250
        val DEFAULT_BACKGROUND_SHADOW_CONCEALMENT_TIME = TimeValue.MILLISECOND * 250
        val DEFAULT_MIN_RADIUS_INTERPOLATOR = DecelerateInterpolator()
        val DEFAULT_MIN_TO_MAX_RADIUS_INTERPOLATOR = AccelerateInterpolator()
        
    }
    
}