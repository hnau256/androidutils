package ru.hnau.androidutils.ui.view.list.group

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.list.base.BaseList
import ru.hnau.androidutils.ui.view.list.base.BaseListItemsDivider
import ru.hnau.androidutils.ui.view.list.base.BaseListOrientation
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.toInt


@SuppressLint("ViewConstructor")
open class GroupList<G : Any, T : Any>(
        context: Context,
        itemsProducer: Producer<List<ListGroup<G, T>>>,
        private val groupsViewWrappersCreator: () -> BaseListViewWrapper<G>,
        private val itemsViewWrappersCreator: () -> BaseListViewWrapper<T>,
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecoration: RecyclerView.ItemDecoration = BaseListItemsDivider(context, orientation)
) : BaseList<GroupListItem<G, T>>(
        context = context,
        fixedSize = fixedSize,
        orientation = orientation,
        itemsDecoration = itemsDecoration,
        itemsProducer = itemsProducer.map {
            it.map {
                val titleList = listOf(GroupListItem.createGroup<G, T>(it.group))
                val itemsList = it.items.map { GroupListItem.createItem<G, T>(it) }
                titleList + itemsList
            }.flatten()
        },
        itemTypeResolver = { it.isGroup.toInt() },
        viewWrappersCreator = { itemType ->
            GroupListViewWrapper.create(
                    itemType > 0,
                    groupsViewWrappersCreator,
                    itemsViewWrappersCreator
            )
        }
)

fun <G : Any, T : Any> groupListView(
        itemsProducer: Producer<List<ListGroup<G, T>>>,
        groupsViewWrappersCreator: (Context) -> BaseListViewWrapper<G>,
        itemsViewWrappersCreator: (Context) -> BaseListViewWrapper<T>,
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecorationGetter: (Context) -> RecyclerView.ItemDecoration = { context -> BaseListItemsDivider(context, orientation) },
        viewConfigurator: (GroupList<G, T>.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { context ->
            GroupList(
                    context,
                    itemsProducer,
                    { groupsViewWrappersCreator.invoke(context) },
                    { itemsViewWrappersCreator.invoke(context) },
                    orientation,
                    fixedSize,
                    itemsDecorationGetter.invoke(context)
            )
        },
        viewConfigurator = viewConfigurator
)

fun <G : Any, T : Any> ViewGroup.addGroupListView(
        itemsProducer: Producer<List<ListGroup<G, T>>>,
        groupsViewWrappers: () -> BaseListViewWrapper<G>,
        itemsViewWrappers: () -> BaseListViewWrapper<T>,
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecorationGetter: RecyclerView.ItemDecoration = BaseListItemsDivider(context, orientation),
        viewConfigurator: (GroupList<G, T>.() -> Unit)? = null
) =
        addView(
                GroupList(
                        context,
                        itemsProducer,
                        groupsViewWrappers,
                        itemsViewWrappers,
                        orientation,
                        fixedSize,
                        itemsDecorationGetter
                ),
                viewConfigurator
        )