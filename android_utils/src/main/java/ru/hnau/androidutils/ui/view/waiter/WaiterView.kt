package ru.hnau.androidutils.ui.view.waiter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.animations.AnimationMetronome
import ru.hnau.androidutils.animations.TwoStatesAnimator
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.detacher.ProducerDetachers
import ru.hnau.jutils.producer.locked_producer.LockedProducer


@SuppressLint("ViewConstructor")
open class WaiterView(
        context: Context,
        private val drawer: WaiterDrawer,
        lockedProducer: LockedProducer,
        visibilitySwitchingTime: TimeValue = DEFAULT_VISIBILITY_SWITCHING_TIME,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : View(
        context,
        attrs,
        defStyleAttr
) {

    companion object {

        val DEFAULT_VISIBILITY_SWITCHING_TIME = TimeValue.SECOND * 0.5

    }

    private var handleTouchEvents = false

    private var visibilityPercentage = 0f
        set(value) {
            field = value
            updateIsAnimating()
        }

    private val visibilityAnimator = TwoStatesAnimator(
            switchingTime = visibilitySwitchingTime
    ).apply {
        attach { visibilityPercentage = it }
    }

    private val animationDetachers = ProducerDetachers()

    private var animating = false
        set(value) = synchronized(this) {
            if (field != value) {
                field = value
                if (value) {
                    AnimationMetronome.attach(animationDetachers) { invalidate() }
                } else {
                    animationDetachers.detachAllAndClear()
                    invalidate()
                }
            }
        }

    private var visibleToUser = false
        set(value) {
            if (field != value) {
                field = value
                updateIsAnimating()
            }
        }

    init {
        observeWhenVisibleToUser(lockedProducer) { isLocked ->
            handleTouchEvents = isLocked
            visibilityAnimator.addNewTarget(isLocked, visibleToUser)
        }
        addIsVisibleToUserHandler { visibleToUser ->
            this.visibleToUser = visibleToUser
        }
    }

    private fun updateIsAnimating() {
        animating = visibilityPercentage > 0.001 && visibleToUser
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        drawer.draw(canvas, visibilityPercentage)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        drawer.setBounds(
                paddingLeft, paddingTop,
                width - paddingRight, height - paddingBottom
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = getDefaultMeasurement(widthMeasureSpec, drawer.preferredWidth + horizontalPaddingSum)
        val height = getDefaultMeasurement(heightMeasureSpec, drawer.preferredHeight + verticalPaddingSum)
        setMeasuredDimension(width, height)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (handleTouchEvents) {
            return true
        }
        return super.onTouchEvent(event)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        visibleToUser = true
    }

    override fun onDetachedFromWindow() {
        visibleToUser = false
        super.onDetachedFromWindow()
    }

}

fun waiter(
        drawer: WaiterDrawer,
        lockedProducer: LockedProducer,
        visibilitySwitchingTime: TimeValue = WaiterView.DEFAULT_VISIBILITY_SWITCHING_TIME,
        viewConfigurator: (WaiterView.() -> Unit)? = null
) = ViewGetter(
        viewCreator = {
            WaiterView(
                    it,
                    drawer,
                    lockedProducer,
                    visibilitySwitchingTime
            )
        },
        viewConfigurator = viewConfigurator
)


fun ViewGroup.addWaiter(
        drawer: WaiterDrawer,
        lockedProducer: LockedProducer,
        visibilitySwitchingTime: TimeValue = WaiterView.DEFAULT_VISIBILITY_SWITCHING_TIME,
        viewConfigurator: (WaiterView.() -> Unit)? = null
) =
        addView(
                WaiterView(
                        context,
                        drawer,
                        lockedProducer,
                        visibilitySwitchingTime
                ),
                viewConfigurator
        )