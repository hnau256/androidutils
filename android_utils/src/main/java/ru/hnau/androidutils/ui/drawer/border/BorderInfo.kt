package ru.hnau.androidutils.ui.drawer.border

import android.graphics.Paint
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.ContextGetter
import ru.hnau.androidutils.context_getters.ExtraSizeGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.drawer.ExtraSize
import ru.hnau.androidutils.ui.utils.BLUR_SIZE_BY_RADIUS


data class BorderInfo(
        val width: DpPxGetter,
        val color: ColorGetter,
        val alpha: Float = 1f,
        val lineJoin: Paint.Join = Paint.Join.MITER,
        val lineCap: Paint.Cap = Paint.Cap.SQUARE
) {

    val extraSize = ExtraSizeGetter { context ->
        ExtraSize(width.getPx(context) / 2)
    }

}