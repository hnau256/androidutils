package ru.hnau.androidutils.ui.utils.types_utils

import android.content.Context
import android.graphics.Color
import android.support.annotation.ColorInt
import android.support.annotation.FloatRange
import android.support.annotation.IntRange
import ru.hnau.androidutils.context_getters.ColorGetter
import kotlin.math.abs

object ColorUtils {

    @ColorInt
    fun colorInterColors(
            @ColorInt colorFrom: Int,
            @ColorInt colorTo: Int,
            @FloatRange(from = 0.0, to = 1.0) pos: Float
    ): Int {

        val alphaFrom = Color.alpha(colorFrom)
        val redFrom = Color.red(colorFrom)
        val greenFrom = Color.green(colorFrom)
        val blueFrom = Color.blue(colorFrom)

        val alphaTo = Color.alpha(colorTo)
        val redTo = Color.red(colorTo)
        val greenTo = Color.green(colorTo)
        val blueTo = Color.blue(colorTo)

        val alpha = alphaFrom + ((alphaTo - alphaFrom) * pos).toInt()
        val red = redFrom + ((redTo - redFrom) * pos).toInt()
        val green = greenFrom + ((greenTo - greenFrom) * pos).toInt()
        val blue = blueFrom + ((blueTo - blueFrom) * pos).toInt()

        return Color.argb(alpha, red, green, blue)
    }

    @ColorInt
    fun colorInterTwoColorGetters(
            context: Context,
            colorGetterFrom: ColorGetter,
            colorGetterTo: ColorGetter,
            @FloatRange(from = 0.0, to = 1.0) pos: Float
    ) =
            colorInterColors(
                    colorFrom = colorGetterFrom.get(context),
                    colorTo = colorGetterTo.get(context),
                    pos = pos
            )

    @ColorInt
    fun colorWithAlpha(
            @ColorInt color: Int, @FloatRange(from = 0.0, to = 1.0) alpha: Float
    ) =
            Color.argb(
                    (Color.alpha(color) * alpha).toInt(),
                    Color.red(color),
                    Color.green(color),
                    Color.blue(color)
            )

    @IntRange(from = 0, to = 255)
    fun extractAlpha(@ColorInt color: Int) =
            Color.alpha(color)

    @ColorInt
    fun removeAlpha(@ColorInt color: Int) =
            Color.rgb(Color.red(color), Color.green(color), Color.blue(color))

    @ColorInt
    fun rgb(r: Float, g: Float, b: Float) =
            Color.rgb((r * 255).toInt(), (g * 255).toInt(), (b * 255).toInt())

    @ColorInt
    fun argb(a: Float, r: Float, g: Float, b: Float) =
            Color.argb((a * 255).toInt(), (r * 255).toInt(), (g * 255).toInt(), (b * 255).toInt())

    @ColorInt
    fun hsl(
            @FloatRange(from = 0.0, to = 1.0) hue: Float,
            @FloatRange(from = 0.0, to = 1.0) saturation: Float,
            @FloatRange(from = 0.0, to = 1.0) light: Float
    ): Int {
        val c = (1f - abs(2 * light - 1f)) * saturation
        val m = light - 0.5f * c
        val x = c * (1f - abs(hue * 6f % 2f - 1f))
        val segment = (hue * 6).toInt()
        return when (segment) {
            0 -> rgb(c + m, x + m, m)
            1 -> rgb(x + m, c + m, m)
            2 -> rgb(m, c + m, x + m)
            3 -> rgb(m, x + m, c + m)
            4 -> rgb(x + m, m, c + m)
            else -> rgb(c + m, m, x + m)
        }
    }

    @ColorInt
    fun hsl(
            @IntRange(from = 0, to = 255) hue: Int,
            @IntRange(from = 0, to = 255) saturation: Int,
            @IntRange(from = 0, to = 255) light: Int
    ) = hsl(
            hue = hue.toFloat() / 255f,
            saturation = saturation.toFloat() / 255f,
            light = light.toFloat() / 255f
    )

}
