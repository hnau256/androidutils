package ru.hnau.androidutils.ui.view.buttons.circle.icon

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButton
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButtonSize
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.setCorrectDrawableLayoutDirection


@SuppressLint("ViewConstructor")
class CircleIconButton(
        context: Context,
        icon: DrawableGetter,
        onClick: () -> Unit,
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT
) : CircleButton(
        context,
        onClick,
        size,
        rippleDrawInfo,
        shadowInfo
) {

    private val icon = icon.get(context).apply {
        callback = this@CircleIconButton
    }

    override fun drawContent(canvas: Canvas) {
        icon.draw(canvas)
    }

    override fun layoutContent(left: Float, top: Float, right: Float, bottom: Float) {

        val iconWidth = icon.intrinsicWidth
        val iconHeight = icon.intrinsicHeight
        val iconLeft = ((left + right - iconWidth) / 2).toInt()
        val iconTop = ((top + bottom - iconHeight) / 2).toInt()

        icon.setBounds(
                iconLeft,
                iconTop,
                iconLeft + iconWidth,
                iconTop + iconHeight
        )
        setCorrectDrawableLayoutDirection(icon)
    }
}

fun circleIconButton(
        icon: DrawableGetter,
        onClick: () -> Unit,
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT,
        viewConfigurator: (CircleIconButton.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { CircleIconButton(it, icon, onClick, size, rippleDrawInfo, shadowInfo) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addCircleIconButton(
        icon: DrawableGetter,
        onClick: () -> Unit,
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT,
        viewConfigurator: (CircleIconButton.() -> Unit)? = null
) =
        addView(
                CircleIconButton(
                        context,
                        icon,
                        onClick,
                        size,
                        rippleDrawInfo,
                        shadowInfo
                ),
                viewConfigurator
        )