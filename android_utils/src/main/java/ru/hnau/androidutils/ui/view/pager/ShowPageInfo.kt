package ru.hnau.androidutils.ui.view.pager


data class ShowPageInfo(
        val pageInfo: PageInfo<*>,
        val clearStack: Boolean
)