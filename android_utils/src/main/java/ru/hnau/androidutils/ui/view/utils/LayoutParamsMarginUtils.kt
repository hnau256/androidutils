package ru.hnau.androidutils.ui.view.utils

import android.content.Context
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


fun ViewGroup.MarginLayoutParams.setMargins(margins: Int) =
        setMargins(margins, margins, margins, margins)

fun ViewGroup.MarginLayoutParams.setMargins(margins: Float) =
        setMargins(margins.toInt())

fun ViewGroup.MarginLayoutParams.setMargins(context: Context, margins: DpPxGetter) =
        setMargins(margins.getPx(context))

fun ViewGroup.MarginLayoutParams.setMargins(left: Float, top: Float, right: Float, bottom: Float) =
        setMargins(left.toInt(), top.toInt(), right.toInt(), bottom.toInt())

fun ViewGroup.MarginLayoutParams.setMargins(context: Context, left: DpPxGetter, top: DpPxGetter, right: DpPxGetter, bottom: DpPxGetter) {
    setMargins(left.getPx(context), top.getPx(context), right.getPx(context), bottom.getPx(context))
}

fun ViewGroup.MarginLayoutParams.setMargins(horizontal: Int, vertical: Int) =
        setMargins(horizontal, vertical, horizontal, vertical)

fun ViewGroup.MarginLayoutParams.setMargins(horizontal: Float, vertical: Float) =
        setMargins(horizontal.toInt(), vertical.toInt())

fun ViewGroup.MarginLayoutParams.setMargins(context: Context, horizontal: DpPxGetter, vertical: DpPxGetter) {
    setMargins(horizontal.getPx(context), vertical.getPx(context))
}

fun ViewGroup.MarginLayoutParams.setHorizontalMargins(horizontalMargins: Int) =
        setMargins(horizontalMargins, topMargin, horizontalMargins, bottomMargin)

fun ViewGroup.MarginLayoutParams.setHorizontalMargins(horizontalMargins: Float) =
        setHorizontalMargins(horizontalMargins.toInt())

fun ViewGroup.MarginLayoutParams.setHorizontalMargins(context: Context, horizontalMargins: DpPxGetter) =
        setHorizontalMargins(horizontalMargins.getPx(context))

fun ViewGroup.MarginLayoutParams.setVerticalMargins(verticalMargins: Int) =
        setMargins(leftMargin, verticalMargins, rightMargin, verticalMargins)

fun ViewGroup.MarginLayoutParams.setVerticalMargins(verticalMargins: Float) =
        setVerticalMargins(verticalMargins.toInt())

fun ViewGroup.MarginLayoutParams.setVerticalMargins(context: Context, verticalMargins: DpPxGetter) =
        setVerticalMargins(verticalMargins.getPx(context))

fun ViewGroup.MarginLayoutParams.setLeftMargin(leftMargin: Int) {
    this.leftMargin = leftMargin
}

fun ViewGroup.MarginLayoutParams.setLeftMargin(leftMargin: Float) =
        setLeftMargin(leftMargin.toInt())

fun ViewGroup.MarginLayoutParams.setLeftMargin(context: Context, leftMargin: DpPxGetter) =
        setLeftMargin(leftMargin.getPx(context))

fun ViewGroup.MarginLayoutParams.setTopMargin(topMargin: Int) {
    this.topMargin = topMargin
}

fun ViewGroup.MarginLayoutParams.setTopMargin(topMargin: Float) =
        setTopMargin(topMargin.toInt())

fun ViewGroup.MarginLayoutParams.setTopMargin(context: Context, topMargin: DpPxGetter) =
        setTopMargin(topMargin.getPx(context))

fun ViewGroup.MarginLayoutParams.setRightMargin(rightMargin: Int) {
    this.rightMargin = rightMargin
}

fun ViewGroup.MarginLayoutParams.setRightMargin(rightMargin: Float) =
        setRightMargin(rightMargin.toInt())

fun ViewGroup.MarginLayoutParams.setRightMargin(context: Context, rightMargin: DpPxGetter) =
        setRightMargin(rightMargin.getPx(context))

fun ViewGroup.MarginLayoutParams.setBottomMargin(bottomMargin: Int) {
    this.bottomMargin = bottomMargin
}

fun ViewGroup.MarginLayoutParams.setBottomMargin(bottomMargin: Float) =
        setBottomMargin(bottomMargin.toInt())

fun ViewGroup.MarginLayoutParams.setBottomMargin(context: Context, bottomMargin: DpPxGetter) =
        setBottomMargin(bottomMargin.getPx(context))