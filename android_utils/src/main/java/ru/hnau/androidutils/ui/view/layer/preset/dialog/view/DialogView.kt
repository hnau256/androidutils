package ru.hnau.androidutils.ui.view.layer.preset.dialog.view

import android.graphics.Canvas
import android.graphics.Rect
import android.view.View
import ru.hnau.androidutils.go_back_handler.GoBackHandler


interface DialogView : GoBackHandler {

    fun build(): View

    override fun handleGoBack(): Boolean = false

    fun onClosed() {}

    val decoration: DialogViewDecoration

}