package ru.hnau.androidutils.ui.view.layer.preset.dialog.view

import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector


class DialogViewBuilder<V : DialogView, I : Any>(
        private val dialogViewCreator: (
                layerManagerConnector: LayerManagerConnector,
                info: I
        ) -> V,
        private val dialogViewConfigurator: V.() -> Unit,
        private val info: I
) {

    fun build(layerManagerConnector: LayerManagerConnector) =
            dialogViewCreator.invoke(layerManagerConnector, info).apply(dialogViewConfigurator)

}