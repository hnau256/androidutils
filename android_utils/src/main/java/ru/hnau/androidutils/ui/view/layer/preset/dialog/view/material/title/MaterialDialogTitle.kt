package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.title

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.setPadding


@SuppressLint("ViewConstructor")
class MaterialDialogTitle(
        context: Context,
        title: StringGetter,
        info: MaterialDialogTitleInfo = MaterialDialogTitleInfo.DEFAULT
) : Label(
        context = context,
        info = info.labelInfo,
        initialText = title
) {

    init {
        setPadding(
                info.paddingHorizontal,
                info.paddingTop,
                info.paddingHorizontal,
                info.paddingBottom
        )
    }

}