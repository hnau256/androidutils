package ru.hnau.androidutils.ui.view.waiter.material.drawer.params

import ru.hnau.androidutils.context_getters.ColorGetter


data class MaterialWaiterColor(
        val foreground: ColorGetter,
        val background: ColorGetter
) {

    companion object {

        val BLACK_ON_TRANSPARENT = MaterialWaiterColor(
                foreground = ColorGetter.BLACK,
                background = ColorGetter.TRANSPARENT
        )

        val BLACK_ON_WHITE = MaterialWaiterColor(
                foreground = ColorGetter.BLACK,
                background = ColorGetter.WHITE
        )

        val WHITE_ON_TRANSPARENT = MaterialWaiterColor(
                foreground = ColorGetter.WHITE,
                background = ColorGetter.TRANSPARENT
        )

        val WHITE_ON_BLACK = MaterialWaiterColor(
                foreground = ColorGetter.WHITE,
                background = ColorGetter.BLACK
        )

        val DEFAULT = BLACK_ON_TRANSPARENT

    }

}