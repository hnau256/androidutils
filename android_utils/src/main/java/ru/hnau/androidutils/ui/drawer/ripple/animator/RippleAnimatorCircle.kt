package ru.hnau.androidutils.ui.drawer.ripple.animator

import android.graphics.PointF
import android.view.animation.Interpolator


class RippleAnimatorCircle(
        private val minInterpolator: Interpolator,
        private val minToMaxInterpolator: Interpolator,
        minTime: Long,
        minToMaxTime: Long,
        private val minPreferredRadius: Float,
        private val maxRadius: Float
) {

    data class DrawInfo(
            var center: PointF = PointF(),
            var radius: Float = 0f,
            var alpha: Float = 0f
    )

    private val drawInfo = DrawInfo()

    private var started: Double = 0.0
    private var minRadius: Float = 0f

    private var minToMaxStarted: Double? = null

    private val minTime = minTime.toDouble()
    private val minToMaxTime = minToMaxTime.toDouble()

    var active: Boolean = false
        private set

    fun newCircle(point: PointF) {
        setPosition(point)
        started = System.currentTimeMillis().toDouble()
        active = true
        minToMaxStarted = null
    }

    fun setPosition(point: PointF) {
        drawInfo.center.set(point)
    }

    private fun getMinEmergenceDrawInfo(): DrawInfo {
        val minPercentage = ((System.currentTimeMillis().toDouble() - started) / minTime).coerceAtMost(1.0)
        drawInfo.alpha = minPercentage.toFloat()
        drawInfo.radius = minPreferredRadius * minInterpolator.getInterpolation(minPercentage.toFloat())
        return drawInfo
    }

    fun startMinToMax() {
        minToMaxStarted = System.currentTimeMillis().toDouble()
        minRadius = getMinEmergenceDrawInfo().radius
    }

    fun getDrawInfoOrDeactivate(): DrawInfo? {
        if (!active) {
            return null
        }

        val minToMaxStarted = this.minToMaxStarted ?: return getMinEmergenceDrawInfo()

        val minToMaxTimePercentage = (System.currentTimeMillis().toDouble() - minToMaxStarted) / minToMaxTime

        if (minToMaxTimePercentage >= 1) {
            active = false
            return null
        }

        drawInfo.alpha = (1 - minToMaxTimePercentage).toFloat()

        val minToMaxAnimationPercentage = minToMaxInterpolator.getInterpolation(minToMaxTimePercentage.toFloat())
        drawInfo.radius = minRadius + (maxRadius - minRadius) * minToMaxAnimationPercentage

        return drawInfo
    }

}