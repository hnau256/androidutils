package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.decoration

import android.view.View
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer


class MaterialDialogViewDecorationBoundsProducer : BoundsProducer() {

    fun onDialogLayout(dialogView: View) = editBounds {
        set(
                dialogView.left.toFloat(),
                dialogView.top.toFloat(),
                dialogView.right.toFloat(),
                dialogView.bottom.toFloat()
        )
    }

}