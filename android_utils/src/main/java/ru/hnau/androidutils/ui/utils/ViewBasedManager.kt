package ru.hnau.androidutils.ui.utils

import android.view.View


@Deprecated("Leak view error")
abstract class ViewBasedManager<V : View> {

    protected var baseView: V? = null

    protected abstract fun generateBaseView(): V

    fun getView() = generateBaseView().also { baseView = it }

}