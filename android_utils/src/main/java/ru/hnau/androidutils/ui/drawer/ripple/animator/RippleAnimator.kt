package ru.hnau.androidutils.ui.drawer.ripple.animator

import android.content.Context
import android.graphics.Canvas
import android.graphics.PointF
import android.view.MotionEvent
import ru.hnau.androidutils.animations.AnimationMetronome
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleInfo
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.handle
import ru.hnau.jutils.producer.Producer


class RippleAnimator(
        context: Context,
        touchHandler: TouchHandler,
        private val rippleInfo: RippleInfo = RippleInfo()
) : Producer<Unit>() {

    private val minCircleRadius = rippleInfo.minCircleRadius.getPx(context)
    private val maxCircleRadius = rippleInfo.maxCircleRadius.getPx(context)

    private val backgroundShadowEmergenceTime = rippleInfo.backgroundShadowEmergenceTime.milliseconds.toFloat()
    private val backgroundShadowConcealmentTime = rippleInfo.backgroundShadowConcealmentTime.milliseconds.toFloat()

    private val circles = ArrayList<RippleAnimatorCircle>()

    private var lastUpDownEventTime = TimeValue.ZERO
    private var lastStateUpDownEventBackgroundAlpha = 0f


    private var down = false
        set(value) {
            if (field != value) {
                lastStateUpDownEventBackgroundAlpha = getShadowAlpha()
                lastUpDownEventTime = TimeValue.now()
                field = value
            }
        }

    private var animating = false
        set(value) = synchronized(this) {
            if (field != value) {
                field = value
                if (value) {
                    AnimationMetronome.attach(this::call)
                } else {
                    AnimationMetronome.detach(this::call)
                }
            }
        }

    private var currentCircle: RippleAnimatorCircle? = null
        set(value) {
            field?.startMinToMax()
            field = value
        }

    init {
        touchHandler.attach(this::onPressedPointChanged)
    }

    fun draw(
            canvas: Canvas,
            circleDrawer: (canvas: Canvas, drawInfo: RippleAnimatorCircle.DrawInfo) -> Unit,
            shadowDrawer: ((canvas: Canvas, alpha: Float) -> Unit)? = null
    ) {
        val shadowDrawn = drawShadow(canvas, shadowDrawer)
        val circlesDrawn = drawCircles(canvas, circleDrawer)
        animating = animating && (shadowDrawn || circlesDrawn)
    }

    private fun drawShadow(
            canvas: Canvas,
            shadowDrawer: ((canvas: Canvas, alpha: Float) -> Unit)? = null
    ): Boolean {
        val shadowAlpha = getShadowAlpha()
        if (shadowAlpha <= 0) {
            return false
        }
        shadowDrawer?.invoke(canvas, shadowAlpha * rippleInfo.maxBackgroundShadowOpacity)
        return true
    }

    private fun drawCircles(
            canvas: Canvas,
            circleDrawer: (canvas: Canvas, drawInfo: RippleAnimatorCircle.DrawInfo) -> Unit
    ): Boolean = synchronized(circles) {
        val circlesDrawn = circles.sumBy { circle ->
            val circleDrawInfo = circle.getDrawInfoOrDeactivate()
            return@sumBy circleDrawInfo?.let {
                circleDrawer.invoke(canvas, circleDrawInfo)
                1
            } ?: 0
        }
        return@synchronized circlesDrawn > 0
    }

    fun getShadowAlpha(): Float {
        val now = System.currentTimeMillis().toDouble()
        val lastUpDownEventTime = lastUpDownEventTime.milliseconds.toDouble()
        val dAlpha = (now - lastUpDownEventTime) /
                (if (down) backgroundShadowEmergenceTime else -backgroundShadowConcealmentTime).toDouble()
        return (lastStateUpDownEventBackgroundAlpha + dAlpha).toFloat().coerceIn(0f, 1f)
    }

    private fun addCircle(point: PointF): RippleAnimatorCircle =
            synchronized(circles) {
                val deactivatedCircle = circles.find { !it.active }
                if (deactivatedCircle != null) {
                    deactivatedCircle.newCircle(point)
                    return@synchronized deactivatedCircle
                }
                val newCircle = RippleAnimatorCircle(
                        minInterpolator = rippleInfo.minRadiusInterpolator,
                        minToMaxInterpolator = rippleInfo.minToMaxRadiusInterpolator,
                        minPreferredRadius = minCircleRadius,
                        maxRadius = maxCircleRadius,
                        minTime = rippleInfo.minCircleEmergenceTime.milliseconds,
                        minToMaxTime = rippleInfo.minToMaxCircleTransformationTime.milliseconds
                )
                circles.add(newCircle)
                newCircle.newCircle(point)
                return@synchronized newCircle
            }

    private fun onPressedPointChanged(pressedPoint: PointF?) {

        if (pressedPoint == null) {
            currentCircle = null
            down = false
            return
        }

        currentCircle.handle(
                ifNotNull = { currentCircle ->
                    currentCircle.setPosition(pressedPoint)
                },
                ifNull = {
                    currentCircle = addCircle(pressedPoint)
                    animating = true
                    down = true
                }
        )
    }

}