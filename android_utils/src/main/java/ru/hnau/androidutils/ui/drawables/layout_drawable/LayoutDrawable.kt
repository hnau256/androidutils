package ru.hnau.androidutils.ui.drawables.layout_drawable

import android.content.Context
import android.graphics.Canvas
import android.graphics.Point
import android.graphics.Rect
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.drawables.ContainerDrawable
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.utils.types_utils.isRTL


open class LayoutDrawable(
        val context: Context,
        initialContent: DrawableGetter = DrawableGetter.EMPTY,
        initialLayoutType: LayoutType = LayoutType.DEFAULT,
        initialGravity: HGravity = HGravity.CENTER
) : ContainerDrawable(
        context,
        initialContent
) {

    companion object

    var layoutType = initialLayoutType
        set(value) {
            if (field != value) {
                field = value
                recalculateContentBounds()
            }
        }

    var gravity = initialGravity
        set(value) {
            if (field != value) {
                field = value
                recalculateContentBounds()
            }
        }

    private val drawRect = Rect()
    private val contentRect = Rect()
    private val contentSize = Point()

    var paddingLeft = 0
        private set

    var paddingTop = 0
        private set

    var paddingRight = 0
        private set

    var paddingBottom = 0
        private set

    override fun getIntrinsicWidth() = contentInner.intrinsicWidth + horizontalPaddingSum
    override fun getIntrinsicHeight() = contentInner.intrinsicHeight + verticalPaddingSum

    override fun draw(canvas: Canvas, width: Float, height: Float) =
            contentInner.draw(canvas)

    fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        paddingLeft = left
        paddingTop = top
        paddingRight = right
        paddingBottom = bottom
        recalculateContentBounds()
    }

    private fun recalculateContentBounds() {
        drawRect.set(
                paddingLeft,
                paddingTop,
                bounds.right - bounds.left - paddingRight,
                bounds.bottom - bounds.top - paddingBottom
        )
        contentSize.set(contentInner.intrinsicWidth, contentInner.intrinsicHeight)
        layoutType.calculatePosition(drawRect, contentSize, gravity, isRTL, contentRect)
        contentInner.bounds = contentRect
    }

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        recalculateContentBounds()
    }

    override fun onContentChanged() {
        super.onContentChanged()
        recalculateContentBounds()
    }

}