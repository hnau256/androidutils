package ru.hnau.androidutils.ui.drawer.shadow.info

import android.graphics.RectF
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.ContextGetter
import ru.hnau.androidutils.context_getters.ExtraSizeGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp2
import ru.hnau.androidutils.context_getters.dp_px.dp4
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.drawer.ExtraSize
import ru.hnau.androidutils.ui.utils.BLUR_SIZE_BY_RADIUS


data class ShadowInfo(
        val offset: DpPxGetter,
        val blur: DpPxGetter,
        val color: ColorGetter,
        val alpha: Float
) {

    companion object {

        val DEFAULT = ShadowInfo(dp4, dp8, ColorGetter.BLACK, 0.5f)

        val DEFAULT_PRESSED = ShadowInfo(dp2, dp4, ColorGetter.BLACK, 0.5f)

    }

    val extraSize = ExtraSizeGetter { context ->
        val blur = blur.getPx(context) * BLUR_SIZE_BY_RADIUS
        val offset = offset.getPx(context)
        ExtraSize(
                left = blur,
                top = (blur - offset).coerceAtLeast(0f),
                right = blur,
                bottom = blur + offset
        )
    }

}