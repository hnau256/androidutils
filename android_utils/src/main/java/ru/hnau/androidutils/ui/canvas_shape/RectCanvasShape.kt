package ru.hnau.androidutils.ui.canvas_shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer


class RectCanvasShape(
        boundsProducer: BoundsProducer
) : CanvasShape(
        boundsProducer
) {

    private var bounds = RectF()

    override fun doClip(canvas: Canvas) {
        canvas.clipRect(bounds)
    }

    override fun doDraw(canvas: Canvas, paint: Paint) {
        canvas.drawRect(bounds, paint)
    }

    override fun invalidate(bounds: RectF) {
        this.bounds = bounds
    }

}