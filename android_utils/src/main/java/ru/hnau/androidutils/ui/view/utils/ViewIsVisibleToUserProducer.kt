package ru.hnau.androidutils.ui.view.utils

import android.arch.lifecycle.*
import android.view.View
import ru.hnau.jutils.producer.DataProducer


class ViewIsVisibleToUserProducer(
        view: View
) : DataProducer<Boolean>(
        false
) {

    companion object {

        private val VISIBLE_TO_USER_LIFECYCLE_EVENTS =
                hashSetOf(Lifecycle.Event.ON_START, Lifecycle.Event.ON_RESUME, Lifecycle.Event.ON_PAUSE)

    }

    var isVisibleToUser: Boolean
        private set(value) {
            if (data != value) {
                data = value
            }
        }
        get() = data

    private val lifecycle = (view.context as? LifecycleOwner)?.lifecycle
            ?: throw IllegalArgumentException("View.context not implements android.arch.lifecycle.LifecycleOwner")

    private val viewIsAttachedToWindowListener =
            object : View.OnAttachStateChangeListener {

                override fun onViewAttachedToWindow(v: View?) =
                        lifecycle.addObserver(lifecycleObserver)

                override fun onViewDetachedFromWindow(v: View?) {
                    lifecycle.removeObserver(lifecycleObserver)
                    isVisibleToUser = false
                }

            }

    private val lifecycleObserver = GenericLifecycleObserver { _, event ->
        isVisibleToUser = event in VISIBLE_TO_USER_LIFECYCLE_EVENTS
    }

    init {
        view.addOnAttachStateChangeListener(viewIsAttachedToWindowListener)
    }

}