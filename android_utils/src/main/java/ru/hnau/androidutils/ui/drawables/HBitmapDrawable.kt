package ru.hnau.androidutils.ui.drawables

import android.graphics.*


open class HBitmapDrawable(val bitmap: Bitmap) : HDrawable() {

    private val drawRect = Rect()

    private val paint = Paint()

    override fun draw(canvas: Canvas, width: Float, height: Float) {
        canvas.drawBitmap(bitmap, null, drawRect, paint)
    }

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {
        super.setBounds(left, top, right, bottom)
        drawRect.set(0, 0, right - left, bottom - top)
    }

    override fun getDrawableOpacity() =
            DrawableOpacity.TRANSLUCENT

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
        paint.colorFilter = colorFilter
    }

}