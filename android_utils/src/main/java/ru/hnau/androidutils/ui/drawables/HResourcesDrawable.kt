package ru.hnau.androidutils.ui.drawables

import android.content.Context
import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.annotation.RequiresApi
import ru.hnau.androidutils.ui.utils.ScaleManager


class HResourcesDrawable(
        context: Context,
        drawableResId: Int
) : Drawable(), Drawable.Callback {

    private val content = context.resources.getDrawable(drawableResId).apply {
        callback = this@HResourcesDrawable
    }

    override fun getIntrinsicWidth() =
            (content.intrinsicWidth * ScaleManager.scaleFactor).toInt()

    override fun getIntrinsicHeight() =
            (content.intrinsicHeight * ScaleManager.scaleFactor).toInt()

    override fun draw(canvas: Canvas) =
            content.draw(canvas)

    override fun setAlpha(alpha: Int) {
        content.alpha = alpha
    }

    override fun getOpacity() =
            content.opacity

    override fun setColorFilter(colorFilter: ColorFilter?) {
        content.colorFilter = colorFilter
    }

    override fun unscheduleDrawable(who: Drawable, what: Runnable) {
        callback?.unscheduleDrawable(who, what)
    }

    override fun invalidateDrawable(who: Drawable) {
        callback?.invalidateDrawable(who)
    }

    override fun scheduleDrawable(who: Drawable, what: Runnable, time: Long) {
        callback?.scheduleDrawable(who, what, time)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setTint(tintColor: Int) =
            content.setTint(tintColor)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setTintList(tint: ColorStateList?) =
            content.setTintList(tint)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setTintMode(tintMode: PorterDuff.Mode) =
            content.setTintMode(tintMode)

    override fun setFilterBitmap(filter: Boolean) {
        content.isFilterBitmap = filter
    }

    override fun setBounds(bounds: Rect) {
        content.bounds = bounds
    }

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) =
            content.setBounds(left, top, right, bottom)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setHotspot(x: Float, y: Float) =
            content.setHotspot(x, y)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setHotspotBounds(left: Int, top: Int, right: Int, bottom: Int) =
            content.setHotspotBounds(left, top, right, bottom)

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun setAutoMirrored(mirrored: Boolean) {
        content.isAutoMirrored = mirrored
    }

    override fun setChangingConfigurations(configs: Int) {
        content.changingConfigurations = configs
    }

    override fun setColorFilter(color: Int, mode: PorterDuff.Mode) =
            content.setColorFilter(color, mode)

    override fun setState(stateSet: IntArray) =
            content.setState(stateSet)

    override fun setVisible(visible: Boolean, restart: Boolean) =
            content.setVisible(visible, restart)

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun canApplyTheme() =
            content.canApplyTheme()

    @RequiresApi(Build.VERSION_CODES.M)
    override fun getLayoutDirection() =
            content.layoutDirection

    override fun getMinimumWidth() =
            (content.minimumWidth * ScaleManager.scaleFactor).toInt()

    override fun getMinimumHeight() =
            (content.minimumHeight * ScaleManager.scaleFactor).toInt()

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun getAlpha() =
            content.alpha

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getDirtyBounds(): Rect =
            content.dirtyBounds

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getColorFilter(): ColorFilter? =
            content.colorFilter

    override fun clearColorFilter() =
            content.clearColorFilter()

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun applyTheme(t: Resources.Theme) =
            content.applyTheme(t)

    override fun getCurrent(): Drawable =
            content.current

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getOutline(outline: Outline) =
            content.getOutline(outline)

    override fun mutate(): Drawable =
            content.mutate()

    override fun jumpToCurrentState() =
            content.jumpToCurrentState()

    override fun getChangingConfigurations() =
            content.changingConfigurations

    override fun isStateful() =
            content.isStateful

    override fun getState(): IntArray =
            content.state

    @RequiresApi(Build.VERSION_CODES.M)
    override fun getHotspotBounds(outRect: Rect) =
            content.getHotspotBounds(outRect)

    override fun getTransparentRegion(): Region? =
            content.transparentRegion

    override fun getConstantState(): ConstantState? =
            content.constantState

    override fun getPadding(padding: Rect) =
            content.getPadding(padding)

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun isAutoMirrored() =
            content.isAutoMirrored

    @RequiresApi(Build.VERSION_CODES.M)
    override fun isFilterBitmap() =
            content.isFilterBitmap

    override fun onLayoutDirectionChanged(layoutDirection: Int) =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                    content.setLayoutDirection(layoutDirection)

}