package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet

import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.decoration.BottomSheetViewDecorationInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.item.BottomSheetItemInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.text.BottomSheetTextInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.title.BottomSheetTitleInfo


data class BottomSheetViewInfo(
        val paddingHorizontal: DpPxGetter = DpPxGetter.ZERO,
        val paddingVertical: DpPxGetter = dp8,
        val title: BottomSheetTitleInfo = BottomSheetTitleInfo.DEFAULT,
        val text: BottomSheetTextInfo = BottomSheetTextInfo.DEFAULT,
        val decoration: BottomSheetViewDecorationInfo = BottomSheetViewDecorationInfo.DEFAULT,
        val item: BottomSheetItemInfo = BottomSheetItemInfo.DEFAULT
) {

    companion object {

        val DEFAULT = BottomSheetViewInfo()

    }

}