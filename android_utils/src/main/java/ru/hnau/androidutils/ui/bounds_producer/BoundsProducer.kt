package ru.hnau.androidutils.ui.bounds_producer

import android.graphics.RectF
import ru.hnau.jutils.ifTrue
import ru.hnau.jutils.producer.CallOnAttachProducer


abstract class BoundsProducer : CallOnAttachProducer<RectF>() {

    private val bounds = RectF()

    private val tempBounds = RectF()

    protected fun editBounds(editor: RectF.() -> Unit) {
        tempBounds.set(bounds)
        editor.invoke(bounds)
        (tempBounds == bounds).ifTrue { return }
        call(bounds)
    }

    override fun getDataForAttachedListener(
            listener: (RectF) -> Unit
    ) = bounds

}