package ru.hnau.androidutils.ui.view.buttons.circle.text

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo
import ru.hnau.androidutils.ui.drawer.shadow.info.ButtonShadowInfo
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButton
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButtonSize
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView


@SuppressLint("ViewConstructor")
class CircleTextButton(
        context: Context,
        text: StringGetter,
        onClick: () -> Unit,
        info: CircleTextButtonInfo = CircleTextButtonInfo(),
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT
) : CircleButton(
        context,
        onClick,
        size,
        rippleDrawInfo,
        shadowInfo
) {

    private val textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        textAlign = Paint.Align.CENTER
        color = info.textColor.get(context)
        info.fontType?.get(context)?.applyToPaint(this)
        textSize = info.textSize.getPxInt(context).toFloat()
    }

    private val text = text.get(context)

    private val drawOffset = -(textPaint.fontMetrics.ascent + textPaint.fontMetrics.descent) / 2f

    private val drawPoint = PointF()

    override fun drawContent(canvas: Canvas) {
        canvas.drawText(text, drawPoint.x, drawPoint.y, textPaint)
    }

    override fun layoutContent(left: Float, top: Float, right: Float, bottom: Float) {
        drawPoint.set(
                (right + left) / 2,
                (bottom + top) / 2 + drawOffset
        )
    }
}

fun circleTextButton(
        text: StringGetter,
        onClick: () -> Unit,
        info: CircleTextButtonInfo = CircleTextButtonInfo(),
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT,
        viewConfigurator: (CircleTextButton.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { CircleTextButton(it, text, onClick, info, size, rippleDrawInfo, shadowInfo) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addCircleTextButton(
        text: StringGetter,
        onClick: () -> Unit,
        info: CircleTextButtonInfo = CircleTextButtonInfo(),
        size: CircleButtonSize = CircleButtonSize.DEFAULT,
        rippleDrawInfo: RippleDrawInfo = RippleDrawInfo(),
        shadowInfo: ButtonShadowInfo = ButtonShadowInfo.DEFAULT,
        viewConfigurator: (CircleTextButton.() -> Unit)? = null
) =
        addView(
                CircleTextButton(
                        context,
                        text,
                        onClick,
                        info,
                        size,
                        rippleDrawInfo,
                        shadowInfo
                ),
                viewConfigurator
        )