package ru.hnau.androidutils.ui.view.waiter

import android.graphics.Canvas


interface WaiterDrawer {

    fun draw(canvas: Canvas, visibilityPercentage: Float)

    fun setBounds(left: Int, top: Int, right: Int, bottom: Int)

    val preferredWidth: Int
    val preferredHeight: Int

}