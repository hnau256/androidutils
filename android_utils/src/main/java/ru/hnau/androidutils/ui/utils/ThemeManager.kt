package ru.hnau.androidutils.ui.utils

import ru.hnau.jutils.producer.Producer

object ThemeManager : Producer<Unit>() {

    fun onThemeChanged() = call(Unit)

}