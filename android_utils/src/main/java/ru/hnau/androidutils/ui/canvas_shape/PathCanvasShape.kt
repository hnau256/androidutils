package ru.hnau.androidutils.ui.canvas_shape

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import ru.hnau.androidutils.ui.bounds_producer.BoundsProducer


abstract class PathCanvasShape(
        boundsProducer: BoundsProducer
) : CanvasShape(
        boundsProducer
) {

    private val path = Path()

    override fun doClip(canvas: Canvas) {
        canvas.clipPath(path)
    }

    override fun doDraw(canvas: Canvas, paint: Paint) {
        canvas.drawPath(path, paint)
    }

    final override fun invalidate(bounds: RectF) {
        invalidatePath(bounds, path)
    }

    protected abstract fun invalidatePath(
            bounds: RectF,
            path: Path
    )

}