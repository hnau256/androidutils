package ru.hnau.androidutils.ui.view.getter

import android.support.constraint.ConstraintLayout
import android.support.design.widget.CoordinatorLayout
import android.support.v4.widget.NestedScrollView
import android.view.View
import android.view.ViewGroup
import android.widget.*

fun ViewGroup.addViewGetter(viewGetter: ViewGetter<*>) =
        addView(viewGetter.get(context))

fun <T : View> ViewGroup.addView(view: T, viewConfigurator: (T.() -> Unit)? = null): T {
    viewConfigurator?.invoke(view)
    addView(view)
    return view
}

fun ViewGroup.addView(viewConfigurator: (View.() -> Unit)? = null) =
        addView(View(context), viewConfigurator)

fun ViewGroup.addImageView(viewConfigurator: (ImageView.() -> Unit)? = null) =
        addView(ImageView(context), viewConfigurator)

fun ViewGroup.addEditText(viewConfigurator: (EditText.() -> Unit)? = null) =
        addView(EditText(context), viewConfigurator)

fun ViewGroup.addLinearLayout(orientation: Int, viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        addView(LinearLayout(context).apply { this.orientation = orientation }, viewConfigurator)

fun ViewGroup.addVerticalLayout(viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        addLinearLayout(LinearLayout.VERTICAL, viewConfigurator)

fun ViewGroup.addHorizontalLayout(viewConfigurator: (LinearLayout.() -> Unit)? = null) =
        addLinearLayout(LinearLayout.HORIZONTAL, viewConfigurator)

fun ViewGroup.addFrameLayout(viewConfigurator: (FrameLayout.() -> Unit)? = null) =
        addView(FrameLayout(context), viewConfigurator)

fun ViewGroup.addRelativeLayout(viewConfigurator: (RelativeLayout.() -> Unit)? = null) =
        addView(RelativeLayout(context), viewConfigurator)

fun ViewGroup.addScrollView(viewConfigurator: (ScrollView.() -> Unit)? = null) =
        addView(ScrollView(context), viewConfigurator)

fun ViewGroup.addHorizontalScrollView(viewConfigurator: (HorizontalScrollView.() -> Unit)? = null) =
        addView(HorizontalScrollView(context), viewConfigurator)

fun ViewGroup.addNestedScrollView(viewConfigurator: (NestedScrollView.() -> Unit)? = null) =
        addView(NestedScrollView(context), viewConfigurator)

fun ViewGroup.addCoordinatorLayout(viewConfigurator: (CoordinatorLayout.() -> Unit)? = null) =
        addView(CoordinatorLayout(context), viewConfigurator)

fun ViewGroup.addConstraintLayout(viewConfigurator: (ConstraintLayout.() -> Unit)? = null) =
        addView(ConstraintLayout(context), viewConfigurator)