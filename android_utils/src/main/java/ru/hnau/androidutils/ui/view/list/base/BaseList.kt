package ru.hnau.androidutils.ui.view.list.base

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.observeWhenVisibleToUser
import ru.hnau.jutils.producer.Producer


@SuppressLint("ViewConstructor")
open class BaseList<T : Any>(
        context: Context,
        private val itemsProducer: Producer<List<T>>,
        private val viewWrappersCreator: (itemType: Int) -> BaseListViewWrapper<T>,
        private val itemTypeResolver: (item: T) -> Int = { 0 },
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecoration: RecyclerView.ItemDecoration = BaseListItemsDivider(context, orientation),
        private val onItemsChangedHandler: ((BaseListViewHolder<T>, BaseListViewHolder<T>) -> Boolean)? = null,
        private val onItemSwipedHandler: ((BaseListViewHolder<T>, SwipeOrDragDirection) -> Unit)? = null,
        private val calculateDiffInfo: BaseListCalculateDiffInfo<T>? = null
) : AbsBaseList<T>(context) {

    override val vertical = orientation.vertical

    private var items: List<T> = emptyList()
        set(value) {
            val oldItems = field
            field = value
            onItemsListChanged(oldItems, value)

        }

    init {
        this.addItemDecoration(itemsDecoration)
        this.setHasFixedSize(fixedSize)
        layoutManager = orientation.createLayoutManager(context)

        observeWhenVisibleToUser(itemsProducer) { items = it }
    }

    @Suppress("UNCHECKED_CAST")
    private fun onItemsListChanged(oldItems: List<T>, newItems: List<T>) {

        val adapter = this.adapter ?: return

        if (calculateDiffInfo == null) {
            adapter.notifyDataSetChanged()
            return
        }

        calculateDiffInfo.onItemsChanged(oldItems, newItems, adapter as Adapter<BaseListViewHolder<T>>)

    }

    override fun getItemsCount() = items.size

    override fun getItem(position: Int) = items[position]

    override fun createNewWrapper(itemType: Int) =
            viewWrappersCreator.invoke(itemType)

    override fun resolveItemType(position: Int) =
            itemTypeResolver.invoke(items[position])

    override fun onItemsChanged(first: BaseListViewHolder<T>, second: BaseListViewHolder<T>): Boolean {
        return onItemsChangedHandler?.invoke(first, second) ?: super.onItemsChanged(first, second)
    }

    override fun onItemSwiped(viewHolder: BaseListViewHolder<T>, direction: SwipeOrDragDirection) {
        super.onItemSwiped(viewHolder, direction)
        onItemSwipedHandler?.invoke(viewHolder, direction)
    }

}

fun <T : Any> listView(
        itemsProducer: Producer<List<T>>,
        viewWrappersCreator: (context: Context, itemType: Int) -> BaseListViewWrapper<T>,
        itemTypeResolver: (item: T) -> Int = { 0 },
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecorationGetter: (Context) -> RecyclerView.ItemDecoration = { context -> BaseListItemsDivider(context, orientation) },
        onItemsChangedHandler: ((BaseListViewHolder<T>, BaseListViewHolder<T>) -> Boolean)? = null,
        onItemSwipedHandler: ((BaseListViewHolder<T>, SwipeOrDragDirection) -> Unit)? = null,
        calculateDiffInfo: BaseListCalculateDiffInfo<T>? = null,
        viewConfigurator: (BaseList<T>.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { context ->
            BaseList(
                    context,
                    itemsProducer,
                    { viewWrappersCreator.invoke(context, it) },
                    itemTypeResolver,
                    orientation,
                    fixedSize,
                    itemsDecorationGetter.invoke(context),
                    onItemsChangedHandler,
                    onItemSwipedHandler,
                    calculateDiffInfo
            )
        },
        viewConfigurator = viewConfigurator
)

fun <T : Any> ViewGroup.addListView(
        itemsProducer: Producer<List<T>>,
        viewWrappers: (itemType: Int) -> BaseListViewWrapper<T>,
        itemTypeResolver: (item: T) -> Int = { 0 },
        orientation: BaseListOrientation = BaseListOrientation.VERTICAL,
        fixedSize: Boolean = true,
        itemsDecoration: RecyclerView.ItemDecoration = BaseListItemsDivider(context, orientation),
        onItemsChangedHandler: ((BaseListViewHolder<T>, BaseListViewHolder<T>) -> Boolean)? = null,
        onItemSwipedHandler: ((BaseListViewHolder<T>, SwipeOrDragDirection) -> Unit)? = null,
        calculateDiffInfo: BaseListCalculateDiffInfo<T>? = null,
        viewConfigurator: (BaseList<T>.() -> Unit)? = null
) =
        addView(
                BaseList(
                        context,
                        itemsProducer,
                        viewWrappers,
                        itemTypeResolver,
                        orientation,
                        fixedSize,
                        itemsDecoration,
                        onItemsChangedHandler,
                        onItemSwipedHandler,
                        calculateDiffInfo
                ),
                viewConfigurator
        )