package ru.hnau.androidutils.ui.view.getter

import android.content.Context
import android.view.View


class ViewGetter<V : View>(
        private val viewCreator: (Context) -> V,
        private val viewConfigurator: (V.() -> Unit)? = null
) {

    fun get(context: Context) =
            viewCreator.invoke(context).apply {
                viewConfigurator?.invoke(this)
            }

}