package ru.hnau.androidutils.ui.view.layer.layer

import android.view.View
import ru.hnau.androidutils.go_back_handler.GoBackHandler


interface Layer : GoBackHandler {

    val view: View

    val transparent: Boolean
        get() = false

    val shading: Boolean
        get() = transparent

    fun afterCreate() {}

    fun beforeDestroy() {}

    override fun handleGoBack() = false

}