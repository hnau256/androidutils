package ru.hnau.androidutils.ui.view.auto_swipe_refresh_view

import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView


class AutoSwipeRefreshView(
        content: View,
        color: ColorGetter,
        private val updateContent: () -> Unit
) : AbstractAutoSwipeRefreshView(
        content.context,
        color
) {

    init {
        addView(content)
    }

    override fun updateContent() = updateContent.invoke()

}

fun autoSwipeRefreshView(
        color: ColorGetter,
        updateContent: () -> Unit,
        viewConfigurator: (AbstractAutoSwipeRefreshView.() -> Unit)? = null
) =
        ViewGetter<AbstractAutoSwipeRefreshView>(
                viewCreator = {
                    object : AbstractAutoSwipeRefreshView(
                            context = it,
                            color = color
                    ) {
                        override fun updateContent() = updateContent.invoke()
                    }
                },
                viewConfigurator = viewConfigurator
        )

fun ViewGroup.addAutoSwipeRefreshView(
        color: ColorGetter,
        updateContent: () -> Unit,
        viewConfigurator: (AbstractAutoSwipeRefreshView.() -> Unit)? = null
) =
        addView(
                object : AbstractAutoSwipeRefreshView(context, color) {
                    override fun updateContent() = updateContent.invoke()
                },
                viewConfigurator
        )