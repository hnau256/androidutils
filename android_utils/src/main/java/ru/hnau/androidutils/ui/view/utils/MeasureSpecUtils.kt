package ru.hnau.androidutils.ui.view.utils

import android.view.View
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


val UNSPECIFIED_MEASURE_SPEC =
        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)

fun makeExactlyMeasureSpec(size: Int) =
        View.MeasureSpec.makeMeasureSpec(size, View.MeasureSpec.EXACTLY)

fun makeAtMostMeasureSpec(size: Int) =
        View.MeasureSpec.makeMeasureSpec(size, View.MeasureSpec.AT_MOST)

fun makeExactlyMeasureSpec(size: Float) = makeExactlyMeasureSpec(size.toInt())
fun makeAtMostMeasureSpec(size: Float) = makeAtMostMeasureSpec(size.toInt())

fun makeExactlyMeasureSpec(size: Double) = makeExactlyMeasureSpec(size.toInt())
fun makeAtMostMeasureSpec(size: Double) = makeAtMostMeasureSpec(size.toInt())

fun makeExactlyMeasureSpec(size: Byte) = makeExactlyMeasureSpec(size.toInt())
fun makeAtMostMeasureSpec(size: Byte) = makeAtMostMeasureSpec(size.toInt())

fun makeExactlyMeasureSpec(size: Long) = makeExactlyMeasureSpec(size.toInt())
fun makeAtMostMeasureSpec(size: Long) = makeAtMostMeasureSpec(size.toInt())

fun makeExactlyMeasureSpec(size: Short) = makeExactlyMeasureSpec(size.toInt())
fun makeAtMostMeasureSpec(size: Short) = makeAtMostMeasureSpec(size.toInt())

fun View.makeExactlyMeasureSpec(size: DpPxGetter) = makeExactlyMeasureSpec(size.getPx(context))
fun View.makeAtMostMeasureSpec(size: DpPxGetter) = makeAtMostMeasureSpec(size.getPx(context))