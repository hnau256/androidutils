package ru.hnau.androidutils.ui.view.utils.touch

import android.graphics.PointF
import android.util.Log
import android.view.MotionEvent
import ru.hnau.androidutils.ui.canvas_shape.CanvasShape
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.producer.*
import ru.hnau.jutils.producer.detacher.ProducerDetachers


class TouchHandler(
        private val canvasShape: CanvasShape,
        private val cancelAfterLeaveCanvasShape: Boolean = false,
        private val onClicked: (() -> Unit)? = null
) : CallOnAttachProducer<PointF?>() {

    private var data: PointF? = null
        set(value) {
            field = value
            call(value)
        }

    private val pressedPoint = PointF()

    val isPressed: Boolean
        get() = data != null

    private val detachers = ProducerDetachers()

    private var isEventInShapeCached: Boolean? = null

    fun handle(event: MotionEvent) {

        isEventInShapeCached = null

        if (event.isDown) {
            pressedPoint.set(event.x, event.y)
            data = pressedPoint.takeIf { isEventInShape(event) }
            return
        }

        isPressed.ifFalse { return }

        if (cancelAfterLeaveCanvasShape && !isEventInShape(event)) {
            data = null
            return
        }

        if (event.isMove) {
            pressedPoint.set(event.x, event.y)
            data = pressedPoint
            return
        }

        if (event.isUp && isEventInShape(event)) {
            onClicked?.invoke()
        }

        if (event.isUpOrCancel) {
            data = null
        }

    }

    private fun isEventInShape(event: MotionEvent): Boolean {
        var eventInShape = isEventInShapeCached
        if (eventInShape == null) {
            eventInShape = canvasShape.contains(event.x, event.y)
            isEventInShapeCached = eventInShape
        }
        return eventInShape
    }

    override fun getDataForAttachedListener(listener: (PointF?) -> Unit) =
            data

    override fun onFirstAttached() {
        super.onFirstAttached()
        canvasShape.attach(detachers) {}
    }

    override fun onLastDetached() {
        detachers.detachAllAndClear()
        super.onLastDetached()
    }

}