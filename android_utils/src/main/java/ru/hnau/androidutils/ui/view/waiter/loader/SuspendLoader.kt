package ru.hnau.androidutils.ui.view.waiter.loader

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import ru.hnau.androidutils.coroutines.createUIJob
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.observeWhenVisibleToUser
import ru.hnau.androidutils.ui.view.view_changer.ViewChanger
import ru.hnau.androidutils.ui.view.view_changer.ViewChangerInfo
import ru.hnau.androidutils.ui.view.waiter.WaiterView
import ru.hnau.jutils.coroutines.SuspendProducer
import ru.hnau.jutils.helpers.Box
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.jutils.producer.locked_producer.SuspendLockedProducer
import ru.hnau.jutils.tryCatchSuspend
import ru.hnau.jutils.tryOrElse


abstract class SuspendLoader<T>(
        context: Context,
        private val producer: SuspendProducer<T>,
        viewChangerInfo: ViewChangerInfo
) : FrameLayout(
        context
) {

    companion object {

        fun <T : Any> create(
                context: Context,
                producer: SuspendProducer<T>,
                waiterViewGenerator: (LockedProducer) -> WaiterView,
                contentViewGenerator: (T) -> View,
                errorViewGenerator: (Throwable) -> View? = { null },
                isNeedUpdateDataResolver: (oldData: T, newData: T) -> Boolean = { oldData: T, newData: T -> oldData != newData },
                info: ViewChangerInfo = ViewChangerInfo()
        ) = object : SuspendLoader<T>(context, producer, info) {

            override fun generateWaiterView(lockedProducer: LockedProducer) =
                    waiterViewGenerator.invoke(lockedProducer)

            override fun generateContentView(data: T) =
                    contentViewGenerator.invoke(data)

            override fun generateErrorView(error: Throwable) =
                    errorViewGenerator.invoke(error)

            override fun isNeedUpdateData(oldData: T, newData: T) =
                    isNeedUpdateDataResolver.invoke(oldData, newData)

        }

    }

    private val viewChanger = ViewChanger(
            context = context,
            info = viewChangerInfo
    )

    private val uiJob = createUIJob()

    private val lockedProducer = SuspendLockedProducer()

    private var oldValue: Box<T>? = null
    private var oldError: Throwable? = null

    init {
        this.addView(viewChanger)
        this.addView(generateWaiterView(lockedProducer))

        observeWhenVisibleToUser(producer) {
            uiJob.launchIfStarted {
                lockedProducer.executeLocked {
                    tryCatchSuspend(
                            throwsAction = this@SuspendLoader::loadNewValueUnsafe,
                            onThrow = this@SuspendLoader::onNewErrorReceived
                    )
                }
            }
        }
    }

    @Throws(Throwable::class)
    private suspend fun loadNewValueUnsafe() {
        val newValue = producer.get()
        onNewValueReceived(newValue)
    }

    private suspend fun onNewErrorReceived(th: Throwable) {
        oldValue = null
        if (oldError == th) {
            return
        }
        oldError = th
        updateView(generateErrorView(th))
    }

    private fun onNewValueReceived(newValue: T) {
        oldError = null
        if (oldValue?.takeIf { !isNeedUpdateData(it.value, newValue) } != null) {
            return
        }
        oldValue = Box(newValue)
        updateView(generateContentView(newValue))
    }

    private fun updateView(view: View?) {
        viewChanger.showView(view)
    }

    protected abstract fun generateWaiterView(lockedProducer: LockedProducer): WaiterView

    protected abstract fun generateContentView(data: T): View

    protected open fun generateErrorView(error: Throwable): View? = null

    protected open fun isNeedUpdateData(oldData: T, newData: T) =
            oldData != newData

}

fun <T : Any> suspendLoader(
        producer: SuspendProducer<T>,
        waiterViewGenerator: (LockedProducer) -> WaiterView,
        contentViewGenerator: (T) -> View,
        errorViewGenerator: (Throwable) -> View? = { null },
        isNeedUpdateDataResolver: (oldData: T, newData: T) -> Boolean = { oldData: T, newData: T -> oldData != newData },
        viewChangerInfo: ViewChangerInfo = ViewChangerInfo(),
        viewConfigurator: (SuspendLoader<T>.() -> Unit)? = null
) =
        ViewGetter(
                viewCreator = {
                    SuspendLoader.create(
                            it,
                            producer,
                            waiterViewGenerator,
                            contentViewGenerator,
                            errorViewGenerator,
                            isNeedUpdateDataResolver,
                            viewChangerInfo
                    )
                },
                viewConfigurator = viewConfigurator
        )


fun <T : Any> ViewGroup.addSuspendLoader(
        producer: SuspendProducer<T>,
        waiterViewGenerator: (LockedProducer) -> WaiterView,
        contentViewGenerator: (T) -> View,
        errorViewGenerator: (Throwable) -> View? = { null },
        isNeedUpdateDataResolver: (oldData: T, newData: T) -> Boolean = { oldData: T, newData: T -> oldData != newData },
        viewChangerInfo: ViewChangerInfo = ViewChangerInfo(),
        viewConfigurator: (SuspendLoader<T>.() -> Unit)? = null
) =
        addView(
                SuspendLoader.create(
                        context,
                        producer,
                        waiterViewGenerator,
                        contentViewGenerator,
                        errorViewGenerator,
                        isNeedUpdateDataResolver,
                        viewChangerInfo
                ),
                viewConfigurator
        )