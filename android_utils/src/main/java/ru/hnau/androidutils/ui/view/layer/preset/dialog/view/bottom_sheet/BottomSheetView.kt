package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet

import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogViewBuilderFabric
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.decoration.BottomSheetViewDecoration
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.item.BottomSheetItem
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.text.BottomSheetText
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.title.BottomSheetTitle
import ru.hnau.androidutils.ui.view.utils.setPadding
import java.util.*


class BottomSheetView(
        val layerManagerConnector: LayerManagerConnector,
        private val info: BottomSheetViewInfo = BottomSheetViewInfo.DEFAULT
) : LinearLayout(
        layerManagerConnector.viewContext
), DialogView {

    companion object : DialogViewBuilderFabric<BottomSheetView, BottomSheetViewInfo>(::BottomSheetView)

    private var title: BottomSheetTitle? = null
    private var text: BottomSheetText? = null

    private val views = LinkedList<View>()

    var goBackHandler: (() -> Boolean)? = null

    private val onClosedListeners = LinkedList<() -> Unit>()

    override val decoration = BottomSheetViewDecoration(context, info.decoration)

    init {
        orientation = VERTICAL
        setPadding(info.paddingHorizontal, info.paddingVertical)
    }

    fun title(title: StringGetter) {
        this.title = BottomSheetTitle(context, title, info.title)
    }

    fun text(text: StringGetter) {
        this.text = BottomSheetText(context, text, info.text)
    }

    fun closeItem(
            text: StringGetter,
            onClick: () -> Unit = {}
    ) = item(text, onClick, true)

    fun item(
            text: StringGetter,
            onClick: () -> Unit
    ) = item(text, onClick, false)

    private fun item(
            text: StringGetter,
            onClick: () -> Unit,
            closeAfterClick: Boolean
    ) {
        view(
                BottomSheetItem(
                        context = context,
                        text = text,
                        onClick = {
                            if (closeAfterClick) {
                                layerManagerConnector.goBack()
                            }
                            onClick.invoke()
                        },
                        info = info.item
                )
        )
    }

    fun view(view: View) {
        this.views.add(view)
    }

    fun addOnClosedListener(onClosedListener: () -> Unit) {
        onClosedListeners.add(onClosedListener)
    }

    fun close() {
        layerManagerConnector.goBack()
    }

    override fun onClosed() {
        super.onClosed()
        synchronized(onClosedListeners) {
            onClosedListeners.forEach { it.invoke() }
            onClosedListeners.clear()
        }
    }

    override fun build(): View {
        removeAllViews()
        title?.let(this::addView)
        text?.let(this::addView)
        views.forEach(this::addView)
        return this
    }

    override fun handleGoBack() =
            goBackHandler?.invoke() == true

}