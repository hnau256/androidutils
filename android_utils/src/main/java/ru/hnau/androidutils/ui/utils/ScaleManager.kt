package ru.hnau.androidutils.ui.utils

import ru.hnau.jutils.producer.Producer

object ScaleManager : Producer<Unit>() {

    private const val DEFAULT_SCALE = 1f

    var scaleFactor: Float = DEFAULT_SCALE
        set(value) {
            if (field != value) {
                field = value
                call(Unit)
            }
        }

}