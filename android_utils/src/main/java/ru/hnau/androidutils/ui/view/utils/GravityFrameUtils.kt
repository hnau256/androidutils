package ru.hnau.androidutils.ui.view.utils

import android.widget.FrameLayout
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity


fun FrameLayout.setCenterForegroundGravity() {
    foregroundGravity = HGravity.CENTER.getGravity(isRTL)
}

fun FrameLayout.setStartTopForegroundGravity() {
    foregroundGravity = HGravity.START_TOP.getGravity(isRTL)
}

fun FrameLayout.setEndTopForegroundGravity() {
    foregroundGravity = HGravity.END_TOP.getGravity(isRTL)
}

fun FrameLayout.setEndBottomForegroundGravity() {
    foregroundGravity = HGravity.END_BOTTOM.getGravity(isRTL)
}

fun FrameLayout.setStartBottomForegroundGravity() {
    foregroundGravity = HGravity.START_BOTTOM.getGravity(isRTL)
}

fun FrameLayout.setTopCenterHorizontalForegroundGravity() {
    foregroundGravity = HGravity.TOP_CENTER_HORIZONTAL.getGravity(isRTL)
}

fun FrameLayout.setBottomCenterHorizontalForegroundGravity() {
    foregroundGravity = HGravity.BOTTOM_CENTER_HORIZONTAL.getGravity(isRTL)
}

fun FrameLayout.setStartCenterVerticalForegroundGravity() {
    foregroundGravity = HGravity.START_CENTER_VERTICAL.getGravity(isRTL)
}

fun FrameLayout.setEndCenterVerticalForegroundGravity() {
    foregroundGravity = HGravity.END_CENTER_VERTICAL.getGravity(isRTL)
}