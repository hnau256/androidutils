package ru.hnau.androidutils.ui.view.waiter.material.drawer

import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterAnimationParams
import ru.hnau.jutils.getDoubleInterDoubles


class MaterialWaiterHelper(
        private val animationParams: MaterialWaiterAnimationParams
) {

    companion object {

        private const val DEGREES_IN_CIRCLE = 360.0

    }

    data class ArcParams(
            var start: Float = 0f,
            var sweep: Float = 0f
    )

    fun calculateArcParams(arcParams: ArcParams) {
        val now = System.currentTimeMillis()
        val startFaze = (now % (animationParams.period.milliseconds)).toFloat() / animationParams.period.milliseconds.toDouble()
        val endFazeRaw = startFaze + animationParams.fazesDistancePercentage
        val endFaze = if (endFazeRaw > 1) endFazeRaw - 1 else endFazeRaw

        val degreesOffset = (now / animationParams.degreesOffsetFactor) % DEGREES_IN_CIRCLE

        val startAngleRaw = fazeToAngle(startFaze, degreesOffset)
        val endAngleRaw = fazeToAngle(endFaze, degreesOffset)

        arcParams.start = startAngleRaw.toFloat()
        arcParams.sweep = (endAngleRaw - startAngleRaw).let { if (it >= 0) it else it + DEGREES_IN_CIRCLE }.toFloat()

    }

    private fun fazeToAngle(faze: Double, degreesOffset: Double): Double {
        val value = faze % 1
        return animationParams.lineSidesInterpolator.getInterpolation((value * value).toFloat()) * DEGREES_IN_CIRCLE + degreesOffset
    }


}