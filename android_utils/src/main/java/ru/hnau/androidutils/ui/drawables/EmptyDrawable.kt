package ru.hnau.androidutils.ui.drawables

import android.graphics.Canvas
import android.graphics.ColorFilter


object EmptyDrawable : HDrawable() {

    override fun draw(canvas: Canvas, width: Float, height: Float) {}

    override fun getDrawableOpacity() = DrawableOpacity.TRANSPARENT

    override fun setAlpha(alpha: Int) {}

    override fun setColorFilter(colorFilter: ColorFilter?) {}

}