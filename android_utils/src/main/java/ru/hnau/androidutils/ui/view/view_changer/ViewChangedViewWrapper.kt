package ru.hnau.androidutils.ui.view.view_changer

import android.graphics.Canvas
import android.graphics.Point
import android.graphics.Rect
import android.view.View
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.utils.types_utils.doInState
import ru.hnau.androidutils.ui.utils.types_utils.offset
import ru.hnau.androidutils.ui.view.utils.getMeasuredSize
import ru.hnau.androidutils.ui.view.utils.layout


class ViewChangedViewWrapper(
        val view: View?,
        val viewChangerInfo: ViewChangerInfo,
        private val isRtlGetter: () -> Boolean
) {

    private val layoutRect = Rect()
    private val parentOffsettedRect = Rect()
    private val viewMeasuredSize = Point()

    fun addToParent(parent: ViewChanger) = view?.let { parent.addView(it) }

    fun removeFromParent(parent: ViewChanger) = view?.let { parent.removeView(it) }

    fun drawViewDecoration(
            canvas: Canvas,
            showing: Boolean,
            showingHidingPercentage: Float,
            onTop: Boolean
    ) {
        view ?: return
        viewChangerInfo.viewDecorationDrawer ?: return

        canvas.doInState {
            canvas.translate(view.left.toFloat(), view.top.toFloat())
            viewChangerInfo.viewDecorationDrawer.draw(
                    canvas = canvas,
                    view = view,
                    scrollFactor = viewChangerInfo.scrollFactor,
                    showing = showing,
                    showingHidingPercentage = showingHidingPercentage,
                    onTop = onTop
            )
        }
    }

    fun measure(widthMeasureSpec: Int, heightMeasureSpec: Int, result: Point) {
        if (view == null) {
            result.set(0, 0)
            return
        }
        view.measure(widthMeasureSpec, heightMeasureSpec)
        view.getMeasuredSize(result)
    }

    fun layout(
            parentRect: Rect,
            fromSide: Side,
            showing: Boolean,
            showingHidingPercentage: Float
    ) {
        val view = this.view ?: return

        val percentage = (if (showing) viewChangerInfo.showInterpolator else viewChangerInfo.hideInterpolator).getInterpolation(showingHidingPercentage)
        val normalizedPercentage = if (showing) 1 - percentage else -percentage

        val offsetX = fromSide.getDirection(isRtlGetter.invoke()).x * parentRect.width() * normalizedPercentage * viewChangerInfo.scrollFactor
        val offsetY = fromSide.getDirection(isRtlGetter.invoke()).y * parentRect.height() * normalizedPercentage * viewChangerInfo.scrollFactor

        parentOffsettedRect.set(parentRect)
        parentOffsettedRect.offset(offsetX, offsetY)
        view.getMeasuredSize(viewMeasuredSize)
        viewChangerInfo.gravity.apply(viewMeasuredSize, parentOffsettedRect, isRtlGetter.invoke(), layoutRect)
        view.layout(layoutRect)

        view.alpha = if (viewChangerInfo.scrollFactor >= 1) 1f else (if (showing) showingHidingPercentage else 1 - showingHidingPercentage)
    }


}