package ru.hnau.androidutils.ui.view.utils

import android.graphics.Point
import android.view.View
import kotlin.math.min


fun getDefaultMeasurement(measureSpec: Int, size: Int): Int {
    var result = size
    val specMode = getMeasuredSpecMode(measureSpec)
    val specSize = getMeasuredSpecSize(measureSpec)

    when (specMode) {
        View.MeasureSpec.AT_MOST -> result = min(size, specSize)
        View.MeasureSpec.UNSPECIFIED -> result = size
        View.MeasureSpec.EXACTLY -> result = specSize
    }
    return result
}

fun getMaxMeasurement(measureSpec: Int, sizeForUnspecified: Int) =
        getMaxMeasurementOrNullIfUnspecified(measureSpec) ?: sizeForUnspecified

fun getMaxMeasurementOrNullIfUnspecified(measureSpec: Int): Int? {
    val specMode = getMeasuredSpecMode(measureSpec)
    return if (specMode == View.MeasureSpec.UNSPECIFIED) {
        null
    } else getMeasuredSpecSize(measureSpec)
}

fun makeMeasureSpec(size: Int, mode: Int) = View.MeasureSpec.makeMeasureSpec(size, mode)

fun View.measure(
        widthMeasureSpecSize: Int,
        widthMeasureSpecMode: Int,
        heightMeasureSpecSize: Int,
        heightMeasureSpecMode: Int
) =
        measure(
                makeMeasureSpec(widthMeasureSpecSize, widthMeasureSpecMode),
                makeMeasureSpec(heightMeasureSpecSize, heightMeasureSpecMode)
        )

fun getMeasuredSpecSize(measureSpec: Int) = View.MeasureSpec.getSize(measureSpec)

fun getMeasuredSpecMode(measureSpec: Int) = View.MeasureSpec.getMode(measureSpec)

fun View.getMeasuredSize(result: Point) = result.set(
        measuredWidth,
        measuredHeight
)

fun View.getMaxSquareMeasuredDimensionSize(widthMeasureSpec: Int, heightMeasureSpec: Int, result: Point) {
    val maxWidth = getMaxMeasurement(widthMeasureSpec, Int.MAX_VALUE) - horizontalPaddingSum
    val maxHeight = getMaxMeasurement(heightMeasureSpec, Int.MAX_VALUE) - verticalPaddingSum
    val preferredSize = min(maxWidth, maxHeight)
    result.set(
            getDefaultMeasurement(widthMeasureSpec, preferredSize + horizontalPaddingSum),
            getDefaultMeasurement(heightMeasureSpec, preferredSize + verticalPaddingSum)
    )
}

fun View.getMeasuredSize() = Point().apply { getMeasuredSize(this) }

fun View.getWidthMeasureSpecWithPadding(widthMeasureSpec: Int) =
        getMeasureSpecWithPadding(widthMeasureSpec, horizontalPaddingSum)

fun View.getHeightMeasureSpecWithPadding(heightMeasureSpec: Int) =
        getMeasureSpecWithPadding(heightMeasureSpec, verticalPaddingSum)

private fun getMeasureSpecWithPadding(measureSpec: Int, orientationPaddingSum: Int): Int {
    val mode = getMeasuredSpecMode(measureSpec)
    if (getMeasuredSpecMode(measureSpec) == View.MeasureSpec.UNSPECIFIED) {
        return measureSpec
    }
    val size = getMeasuredSpecSize(measureSpec) - orientationPaddingSum
    return makeMeasureSpec(size, mode)
}