package ru.hnau.androidutils.ui.view.layer.preset.dialog

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.View
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogViewDecoration
import ru.hnau.androidutils.ui.view.utils.setSoftwareRendering


@SuppressLint("ViewConstructor")
class DialogLayerDecorationView(
        context: Context,
        private val decorationGetter: () -> DialogViewDecoration
) : View(context) {

    init {
        setSoftwareRendering()
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        decorationGetter.invoke().draw(canvas)
    }

}