package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RoundCornersRectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.font_type.createPaint
import ru.hnau.androidutils.ui.view.utils.getDefaultMeasurement
import ru.hnau.androidutils.ui.view.utils.horizontalPaddingSum
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler
import ru.hnau.androidutils.ui.view.utils.verticalPaddingSum


@SuppressLint("ViewConstructor")
class MaterialDialogButton(
        context: Context,
        text: StringGetter,
        onClick: () -> Unit,
        private val info: MaterialDialogButtonInfo,
        textColor: ColorGetter = info.textColor
) : View(
        context
) {

    private val text = text.get(context).toUpperCase()

    private val textPaint = info.textFont.createPaint(context).apply {
        color = textColor.get(context)
        textSize = info.textSize.getPxInt(context).toFloat()
        textAlign = Paint.Align.CENTER
    }

    private val spaceX = info.paddingHorizontal.getPxInt(context)

    private val preferredWidth = textPaint.measureText(this.text).toInt() + spaceX * 2
    private val preferredHeight = info.height.getPxInt(context)

    private val drawTextPoint = PointF()

    private val boundsProducer = ViewBoundsProducer(this)

    private val canvasShape = RoundCornersRectCanvasShape(
            context = context,
            boundsProducer = boundsProducer,
            cornersRadius = info.borderRadius
    )

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClicked = onClick
    )

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            canvasShape = canvasShape,
            rippleDrawInfo = info.rippleDrawInfo,
            touchHandler = touchHandler
    )

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        rippleDrawer.draw(canvas)
        canvas.drawText(text, drawTextPoint.x, drawTextPoint.y, textPaint)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        drawTextPoint.set(
                paddingLeft + (width - horizontalPaddingSum) / 2f,
                paddingTop + (height - verticalPaddingSum + textPaint.textSize - textPaint.fontMetrics.descent) / 2f
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        setMeasuredDimension(
                getDefaultMeasurement(widthMeasureSpec, preferredWidth),
                getDefaultMeasurement(heightMeasureSpec, preferredHeight)
        )
    }


}