package ru.hnau.androidutils.ui.font_type

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import ru.hnau.androidutils.context_getters.ContextGetter


class FontTypeGetter(
        getter: (context: Context) -> FontType
) : ContextGetter<FontType>(
        getter = getter,
        dependencies = emptyList()
) {

    companion object {

        val DEFAULT = FontTypeGetter(FontType.DEFAULT)
        val DEFAULT_BOLD = FontTypeGetter(FontType.DEFAULT_BOLD)
        val MONOSPACE = FontTypeGetter(FontType.MONOSPACE)
        val SANS_SERIF = FontTypeGetter(FontType.SANS_SERIF)
        val SERIF = FontTypeGetter(FontType.SERIF)

    }

    constructor(fontType: FontType) : this({ fontType })

    constructor(typeface: Typeface) : this(FontType(typeface))

    constructor(
            assertFileName: String
    ) : this(
            { context -> FontType(context, assertFileName) }
    )

}

fun FontTypeGetter?.createPaint(context: Context) = this?.get(context).createPaint()

fun Paint.setFontTypeGetter(context: Context, fontTypeGetter: FontTypeGetter?) =
        setFontType(fontTypeGetter?.get(context))