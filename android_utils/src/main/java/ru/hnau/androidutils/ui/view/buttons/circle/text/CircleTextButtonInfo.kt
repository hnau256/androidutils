package ru.hnau.androidutils.ui.view.buttons.circle.text

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter


import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.ui.font_type.FontTypeGetter


data class CircleTextButtonInfo(
        val textSize: DpPxGetter = DEFAULT_TEXT_SIZE,
        val fontType: FontTypeGetter? = null,
        val textColor: ColorGetter = DEFAULT_TEXT_COLOR
) {

    companion object {

        val DEFAULT_TEXT_SIZE = dp16
        val DEFAULT_TEXT_COLOR = ColorGetter.BLACK

    }

}