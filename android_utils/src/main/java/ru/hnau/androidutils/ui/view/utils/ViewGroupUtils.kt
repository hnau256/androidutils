package ru.hnau.androidutils.ui.view.utils

import android.view.View
import android.view.ViewGroup

fun ViewGroup.addViewIfNotNull(view: View?) =
        view?.let(this::addView)

fun ViewGroup.forEachChildren(action: (View) -> Unit) =
        (0 until childCount).map(this::getChildAt).forEach(action)

fun ViewGroup.forEachChildrenIndexed(action: (Int, View) -> Unit) =
        (0 until childCount).map(this::getChildAt).forEachIndexed(action)