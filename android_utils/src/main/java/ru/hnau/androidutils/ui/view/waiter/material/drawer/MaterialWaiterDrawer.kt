package ru.hnau.androidutils.ui.view.waiter.material.drawer

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import ru.hnau.androidutils.ui.view.waiter.WaiterDrawer
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterAnimationParams
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterColor
import ru.hnau.androidutils.ui.view.waiter.material.drawer.params.MaterialWaiterSize
import ru.hnau.androidutils.ui.utils.types_utils.ColorUtils


class MaterialWaiterDrawer(
        context: Context,
        color: MaterialWaiterColor,
        size: MaterialWaiterSize,
        animationParams: MaterialWaiterAnimationParams
) : WaiterDrawer {

    private val radius = size.radius.getPx(context)

    private val preferredSize = size.preferredSize.getPxInt(context)

    override val preferredWidth = preferredSize
    override val preferredHeight = preferredSize

    private val lineColor = color.foreground.get(context)
    private val lineAlpha = ColorUtils.extractAlpha(lineColor)
    private val linePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        strokeWidth = size.lineWidth.getPx(context)
        strokeCap = size.lineCap
        this.color = ColorUtils.removeAlpha(lineColor)
    }

    private val backgroundColor = color.background.get(context)
    private val backgroundAlpha = ColorUtils.extractAlpha(backgroundColor)
    private val backgroundPaint = Paint().apply {
        this.color = ColorUtils.removeAlpha(backgroundColor)
    }

    private val bounds = Rect()
    private val lineArcOvalRect = RectF()

    private val helper = MaterialWaiterHelper(animationParams)
    private val arcParams = MaterialWaiterHelper.ArcParams()

    override fun setBounds(left: Int, top: Int, right: Int, bottom: Int) {

        bounds.set(left, top, right, bottom)

        val centerX = (left + right) / 2f
        val centerY = (top + bottom) / 2f
        lineArcOvalRect.set(
                centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius
        )

    }

    override fun draw(
            canvas: Canvas,
            visibilityPercentage: Float
    ) {

        backgroundPaint.alpha = (backgroundAlpha * visibilityPercentage).toInt()
        canvas.drawRect(bounds, backgroundPaint)

        helper.calculateArcParams(arcParams)
        linePaint.alpha = (lineAlpha * visibilityPercentage).toInt()
        canvas.drawArc(lineArcOvalRect, arcParams.start, arcParams.sweep, false, linePaint)

    }
}