package ru.hnau.androidutils.ui.view.list.group


data class ListGroup<G: Any, T: Any>(
        val group: G,
        val items: List<T>
)