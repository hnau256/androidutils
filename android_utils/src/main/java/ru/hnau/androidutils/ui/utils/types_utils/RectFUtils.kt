package ru.hnau.androidutils.ui.utils.types_utils

import android.graphics.Point
import android.graphics.PointF
import android.graphics.Rect
import android.graphics.RectF


fun RectF.toRect() = Rect(left.toInt(), top.toInt(), right.toInt(), bottom.toInt())

fun RectF.offset(point: PointF) = offset(point.x, point.y)

fun RectF.offset(dx: Int, dy: Int) = offset(dx.toFloat(), dy.toFloat())

fun RectF.offset(point: Point) = offset(point.x, point.y)

fun RectF.getSize(point: PointF) = point.set(width(), height())

fun RectF.getSize() = PointF().apply { getSize(this) }

fun RectF.getPos(point: PointF) = point.set(left, top)

fun RectF.getPos() = PointF().apply { getPos(this) }

fun RectF.getCenter(point: PointF) = point.set(centerX(), centerY())

fun RectF.getCenter() = PointF().apply { getCenter(this) }