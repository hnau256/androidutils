package ru.hnau.androidutils.ui.view.auto_swipe_refresh_view

import android.content.Context
import android.support.v4.widget.SwipeRefreshLayout
import ru.hnau.androidutils.context_getters.ColorGetter


abstract class AbstractAutoSwipeRefreshView(
        context: Context,
        color: ColorGetter = ColorGetter.BLACK
) : SwipeRefreshLayout(
        context
) {

    init {
        this.setColorSchemeColors(color.get(context))
        this.setOnRefreshListener {
            isRefreshing = false
            updateContent()
        }
    }

    protected abstract fun updateContent()

}