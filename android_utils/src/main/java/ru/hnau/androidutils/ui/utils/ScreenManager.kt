package ru.hnau.androidutils.ui.utils

import android.os.Build
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.pxInt


object ScreenManager {

    val width = pxInt { it.resources.displayMetrics.widthPixels }
    val height = pxInt { it.resources.displayMetrics.heightPixels }

    val statusBarHeight = pxInt { context ->

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return@pxInt 0
        }

        val resources = context.resources
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId <= 0) {
            return@pxInt 0
        }

        resources.getDimensionPixelSize(resourceId)

    }

}