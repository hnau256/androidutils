package ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material

import android.annotation.SuppressLint
import android.view.View
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.DialogViewBuilderFabric
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button.MaterialDialogButtons
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.text.MaterialDialogText
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.title.MaterialDialogTitle
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.decoration.MaterialDialogViewDecoration
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.jutils.getter.Getter
import java.util.*


@SuppressLint("ViewConstructor")
class MaterialDialogView(
        val layerManagerConnector: LayerManagerConnector,
        private val info: MaterialDialogViewInfo
) : LinearLayout(layerManagerConnector.viewContext), DialogView {

    companion object : DialogViewBuilderFabric<MaterialDialogView, MaterialDialogViewInfo>(::MaterialDialogView)

    private val buttons = Getter { MaterialDialogButtons(context, info.button) }

    private var title: MaterialDialogTitle? = null
    private var text: MaterialDialogText? = null

    private val views = LinkedList<View>()

    var goBackHandler: (() -> Boolean)? = null

    private val onClosedListeners = LinkedList<() -> Unit>()

    override val decoration = MaterialDialogViewDecoration(context, info.decoration)

    init {
        orientation = VERTICAL
        setPadding(info.paddingHorizontal, info.paddingVertical)
    }

    fun title(title: StringGetter) {
        this.title = MaterialDialogTitle(context, title, info.title)
    }

    fun text(text: StringGetter) {
        this.text = MaterialDialogText(context, text, info.text)
    }

    fun view(view: View) {
        this.views.add(view)
    }

    fun closeButton(
            text: StringGetter,
            textColor: ColorGetter = info.button.textColor,
            onClick: () -> Unit = {}
    ) = button(text, textColor, onClick, true)

    fun button(
            text: StringGetter,
            textColor: ColorGetter = info.button.textColor,
            onClick: () -> Unit
    ) = button(text, textColor, onClick, false)

    fun button(
            text: StringGetter,
            textColor: ColorGetter,
            onClick: () -> Unit,
            closeAfterClick: Boolean
    ) {
        buttons.get().addButton(
                text = text,
                onClick = {
                    if (closeAfterClick) {
                        layerManagerConnector.goBack()
                    }
                    onClick.invoke()
                },
                textColor = textColor
        )
    }

    fun addOnClosedListener(onClosedListener: () -> Unit) {
        onClosedListeners.add(onClosedListener)
    }

    fun close() {
        layerManagerConnector.goBack()
    }

    override fun onClosed() {
        super.onClosed()
        synchronized(onClosedListeners) {
            onClosedListeners.forEach { it.invoke() }
            onClosedListeners.clear()
        }
    }

    override fun build(): View {
        removeAllViews()
        title?.let(this::addView)
        text?.let(this::addView)
        views.forEach(this::addView)
        buttons.getExistence()?.let(this::addView)
        return this
    }

    override fun handleGoBack() =
            goBackHandler?.invoke() == true

}