package ru.hnau.androidutils.preferences

import android.content.SharedPreferences


class PreferencesIntProperty(
        preferences: SharedPreferences,
        key: String,
        defaultValue: Int = 0
) : PreferencesProperty<Int>(
        preferences = preferences,
        key = key,
        defaultValue = defaultValue,
        mutableType = false
) {

    override fun readValue(container: SharedPreferences, key: String) =
            container.getInt(key, defaultValue)

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Int) {
        editor.putInt(key, value)
    }
}