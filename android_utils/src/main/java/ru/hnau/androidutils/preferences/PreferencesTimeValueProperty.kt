package ru.hnau.androidutils.preferences

import android.content.SharedPreferences
import ru.hnau.jutils.TimeValue


class PreferencesTimeValueProperty(
        preferences: SharedPreferences,
        key: String,
        defaultValue: TimeValue = TimeValue.ZERO
) : PreferencesProperty<TimeValue>(
        preferences = preferences,
        key = key,
        defaultValue = defaultValue,
        mutableType = false
) {

    override fun readValue(container: SharedPreferences, key: String) =
            TimeValue(container.getLong(key, defaultValue.milliseconds))

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: TimeValue) {
        editor.putLong(key, value.milliseconds)
    }
}