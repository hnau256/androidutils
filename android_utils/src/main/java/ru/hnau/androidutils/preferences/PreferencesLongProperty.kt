package ru.hnau.androidutils.preferences

import android.content.SharedPreferences


class PreferencesLongProperty(
        preferences: SharedPreferences,
        key: String,
        defaultValue: Long = 0L
) : PreferencesProperty<Long>(
        preferences = preferences,
        key = key,
        defaultValue = defaultValue,
        mutableType = false
) {

    override fun readValue(container: SharedPreferences, key: String) = container.getLong(key, defaultValue)

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Long) {
        editor.putLong(key, value)
    }
}