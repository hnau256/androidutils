package ru.hnau.androidutils.preferences

import android.content.Context
import android.content.SharedPreferences
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.jutils.TimeValue


abstract class PreferencesManager(
        private val categoryPrefix: String
) {

    companion object {

        private val preferences: SharedPreferences =
                ContextConnector.context.getSharedPreferences("PREFERENCES", Context.MODE_PRIVATE)

    }

    private fun getKey(suffix: String) = "${categoryPrefix}_$suffix"

    protected fun newIntProperty(key: String, defaultValue: Int = 0) =
            PreferencesIntProperty(preferences, getKey(key), defaultValue)

    protected fun newStringProperty(key: String, defaultValue: String = "") =
            PreferencesStringProperty(preferences, getKey(key), defaultValue)

    protected fun newStringSetProperty(key: String, defaultValue: Set<String> = emptySet()) =
            PreferencesStringSetProperty(preferences, getKey(key), defaultValue)

    protected fun newLongProperty(key: String, defaultValue: Long = 0L) =
            PreferencesLongProperty(preferences, getKey(key), defaultValue)

    protected fun newFloatProperty(key: String, defaultValue: Float = 0f) =
            PreferencesFloatProperty(preferences, getKey(key), defaultValue)

    protected fun newBooleanProperty(key: String, defaultValue: Boolean = false) =
            PreferencesBooleanProperty(preferences, getKey(key), defaultValue)

    protected fun newTimeValueProperty(key: String, defaultValue: TimeValue = TimeValue.ZERO) =
            PreferencesTimeValueProperty(preferences, getKey(key), defaultValue)

}