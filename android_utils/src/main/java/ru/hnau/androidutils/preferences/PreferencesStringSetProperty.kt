package ru.hnau.androidutils.preferences

import android.content.SharedPreferences


class PreferencesStringSetProperty(
        preferences: SharedPreferences,
        key: String,
        defaultValue: Set<String> = emptySet()
) : PreferencesProperty<Set<String>>(
        preferences = preferences,
        key = key,
        defaultValue = defaultValue,
        mutableType = true
) {

    override fun readValue(container: SharedPreferences, key: String): Set<String> =
            container.getStringSet(key, defaultValue)!!

    override fun writeValue(editor: SharedPreferences.Editor, key: String, value: Set<String>) {
        editor.putStringSet(key, value)
    }
}