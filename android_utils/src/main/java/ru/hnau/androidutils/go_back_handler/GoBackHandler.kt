package ru.hnau.androidutils.go_back_handler


interface GoBackHandler {

    fun handleGoBack(): Boolean

    companion object {

        fun handleGoBack(possibleHandler: Any) =
                (possibleHandler as? GoBackHandler)?.handleGoBack() ?: false

    }

}