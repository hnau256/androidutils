package ru.hnau.androidutils.utils

import android.widget.Toast
import ru.hnau.androidutils.context_getters.StringGetter


fun showToast(
        text: StringGetter,
        duration: ToastDuration = ToastDuration.SHORT
) {
    val context = ContextConnector.context
    Toast.makeText(
            context,
            text.get(context),
            duration.toastValue
    ).show()
}

fun longToast(text: StringGetter) = showToast(text, duration = ToastDuration.LONG)
fun shortToast(text: StringGetter) = showToast(text, duration = ToastDuration.SHORT)

enum class ToastDuration(
        val toastValue: Int
) {
    SHORT(Toast.LENGTH_SHORT),
    LONG(Toast.LENGTH_LONG)
}