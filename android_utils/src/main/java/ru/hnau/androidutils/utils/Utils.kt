package ru.hnau.androidutils.utils

import android.os.SystemClock
import ru.hnau.jutils.TimeValue
import java.util.*

val RANDOM = Random(System.currentTimeMillis())

fun generateId() = RANDOM.nextInt() and 0x0000ffff

fun getAnimationPercentage(period: TimeValue) =
        (System.currentTimeMillis() % period.milliseconds).toFloat() / period.milliseconds.toFloat()

fun TimeValue.Companion.elapsedRealtime() =
        TimeValue(SystemClock.elapsedRealtime())

@Deprecated("Use from JUtils")
inline fun <T> Iterable<T>.findLastPos(predicate: (T) -> Boolean): Int? {
    reversed().forEachIndexed { pos, element -> if (predicate.invoke(element)) return pos }
    return null
}