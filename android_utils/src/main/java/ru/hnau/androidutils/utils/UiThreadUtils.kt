package ru.hnau.androidutils.utils

import android.os.Handler
import android.os.Looper
import android.view.View
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.finisher.Finisher
import ru.hnau.jutils.producer.Producer
import ru.hnau.jutils.producer.WrapProducer

val MAIN_LOOPER = Looper.getMainLooper()!!
private val MAIN_THREAD_HANDLER = Handler(MAIN_LOOPER)

fun runUi(action: () -> Unit) {
    if (runIfUi(action) != null) {
        return
    }
    MAIN_THREAD_HANDLER.post(action)
}

inline fun <T : Any> runIfUi(action: () -> T): T? {
    val isMainThread = Looper.myLooper() == MAIN_LOOPER
    if (isMainThread) {
        return action.invoke()
    }
    return null
}

fun <T : Any> finishUi(action: () -> T) = Finisher<T> { onFinished ->
    runUi { onFinished.invoke(action.invoke()) }
}

fun <T : Any> Finisher<T>.mapUi() =
        Finisher<T> { onFinished -> await { data -> runUi { onFinished.invoke(data) } } }

fun <T : Any> Producer<T>.mapUi() = object : WrapProducer<T, T>(this) {
    override fun onWrappedProducerCall(data: T) {
        runUi { call(data) }
    }
}

fun Handler.postDelayed(pause: TimeValue, action: () -> Unit) =
        postDelayed(action, pause.milliseconds)