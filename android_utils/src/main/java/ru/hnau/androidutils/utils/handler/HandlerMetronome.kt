package ru.hnau.androidutils.utils.handler

import android.os.Handler
import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.producer.Producer


class HandlerMetronome(
        initialPeriod: TimeValue
) : Producer<Unit>() {

    companion object {

        private val INITIAL_STEP = Runnable { }

    }

    var period = TimeValue.MILLISECOND
        set(value) {
            field = if (value.milliseconds < 1) TimeValue.MILLISECOND else value
        }

    private val handler = Handler()

    var active = false
        set(value) {
            synchronized(this) {
                if (field == value) {
                    return@synchronized
                }
                field = value
                if (value) {
                    step.run()
                } else {
                    handler.removeCallbacks(step)
                }
            }
        }

    private var step = INITIAL_STEP

    init {
        period = initialPeriod

        step = Runnable {
            synchronized(this@HandlerMetronome) {
                if (!active) {
                    return@Runnable
                }
            }
            handler.postDelayed(step, period.milliseconds)
            call(Unit)
        }
    }

    fun start() {
        active = true
    }

    fun stop() {
        active = false
    }


}