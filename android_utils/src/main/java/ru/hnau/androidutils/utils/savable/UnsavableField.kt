package ru.hnau.androidutils.utils.savable

@Target(AnnotationTarget.FIELD)
@Retention(AnnotationRetention.RUNTIME)
annotation class UnsavableField