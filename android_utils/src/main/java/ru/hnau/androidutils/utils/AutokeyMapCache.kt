package ru.hnau.androidutils.utils


open class AutokeyMapCache<K, V>(
        private val keysGenerator: () -> K,
        private val tryCount: Int = 1000
) {

    private val container = HashMap<K, V>()

    fun put(data: V): K? {
        var key: K
        var tryCount = 0
        do {
            key = keysGenerator.invoke()
            tryCount++
        } while (container[key] != null && tryCount < this.tryCount)

        if (tryCount >= this.tryCount) {
            return null
        }

        container[key] = data
        return key
    }

    fun get(key: K) = container[key]

    fun remove(key: K) = container.remove(key)

}