package ru.hnau.androidutils.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.support.annotation.RequiresApi
import ru.hnau.jutils.tryCatch
import java.io.Serializable
import java.lang.reflect.ParameterizedType


fun Context.browse(uri: String) =
        tryCatch { startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri))) }

inline fun <reified T : Activity> Context.startActivity(intentConfigurator: Intent.() -> Unit = {}) =
        startActivity(Intent(this, T::class.java).apply(intentConfigurator))

@Suppress("UNCHECKED_CAST")
fun Intent.put(vararg extras: Pair<String, Any>) = extras.forEach { (name, value) ->
    when (value) {
        is String -> putExtra(name, value)
        is CharSequence -> putExtra(name, value)
        is Char -> putExtra(name, value)
        is Int -> putExtra(name, value)
        is Long -> putExtra(name, value)
        is Short -> putExtra(name, value)
        is Byte -> putExtra(name, value)
        is Float -> putExtra(name, value)
        is Double -> putExtra(name, value)
        is Bundle -> putExtra(name, value)
        is Boolean -> putExtra(name, value)
        is IntArray -> putExtra(name, value)
        is CharArray -> putExtra(name, value)
        is BooleanArray -> putExtra(name, value)
        is ByteArray -> putExtra(name, value)
        is ShortArray -> putExtra(name, value)
        is LongArray -> putExtra(name, value)
        is DoubleArray -> putExtra(name, value)
        is FloatArray -> putExtra(name, value)
        is Parcelable -> putExtra(name, value)
        is Serializable -> putExtra(name, value)

        is Array<*> ->
            when (value.javaClass.componentType) {
                Parcelable::class.java -> putExtra(name, value as Array<Parcelable>)
                String::class.java -> putExtra(name, value as Array<String>)
                CharSequence::class.java -> putExtra(name, value as Array<CharSequence>)
                else -> throwNotSupportedTypeException(name, value)
            }

        is ArrayList<*> ->
            when ((value.javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0]) {
                Parcelable::class.java -> putParcelableArrayListExtra(name, value as ArrayList<Parcelable>)
                Int::class.java -> putIntegerArrayListExtra(name, value as ArrayList<Int>)
                String::class.java -> putStringArrayListExtra(name, value as ArrayList<String>)
                CharSequence::class.java -> putCharSequenceArrayListExtra(name, value as ArrayList<CharSequence>)
                else -> throwNotSupportedTypeException(name, value)
            }

        else -> throwNotSupportedTypeException(name, value)
    }
}

private fun throwNotSupportedTypeException(name: String, value: Any): Nothing =
        throw IllegalArgumentException("class (${value.javaClass.name}) of value $name not supported for Intent extra")

fun Intent.noHistory() = addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
fun Intent.singleTop() = addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
fun Intent.newTask() = addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
fun Intent.multipleTask() = addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
fun Intent.clearTop() = addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
fun Intent.forwardResult() = addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT)
fun Intent.previousIsTop() = addFlags(Intent.FLAG_ACTIVITY_PREVIOUS_IS_TOP)
fun Intent.excludeFromRecents() = addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
fun Intent.broughtToFront() = addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT)
fun Intent.resetTaskIfNeeded() = addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
fun Intent.launchedFromHistory() = addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY)
fun Intent.noUserAction() = addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION)
fun Intent.reorderToFront() = addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
fun Intent.noAnimation() = addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
fun Intent.clearTask() = addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
fun Intent.taskOnHome() = addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME)

@Suppress("DeprecatedCallableAddReplaceWith")
@Deprecated("As of API 21 this performs identically to")
fun Intent.clearWhenTaskReset(): Intent = addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Intent.newDocument() = addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT)

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun Intent.retainInRecents() = addFlags(Intent.FLAG_ACTIVITY_RETAIN_IN_RECENTS)

@RequiresApi(Build.VERSION_CODES.N)
fun Intent.launchAdjacent() = addFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT)

@RequiresApi(Build.VERSION_CODES.P)
fun Intent.matchExternal() = addFlags(Intent.FLAG_ACTIVITY_MATCH_EXTERNAL)