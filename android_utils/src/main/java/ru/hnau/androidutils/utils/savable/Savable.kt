package ru.hnau.androidutils.utils.savable

import android.os.Bundle
import android.os.Parcelable
import android.util.SparseArray
import java.io.Serializable
import java.lang.reflect.Field
import java.lang.reflect.ParameterizedType


@Deprecated("Not safe")
abstract class Savable {

    companion object {

        private fun createSavableInstance(clazz: Class<out Savable>): Savable {
            val primaryConstructor = clazz.constructors.find { it.parameterTypes.isEmpty() }
                    ?: throw IllegalArgumentException("Cannot create instance of ${clazz.simpleName}, there is no primary constructor")
            return primaryConstructor.newInstance() as Savable
        }

        fun restoreFromBundle(bundle: Bundle, key: String, clazz: Class<out Savable>): Savable {
            val instance = createSavableInstance(clazz)
            instance.restoreInstanceState(bundle, key)
            return instance
        }

    }

    private val savableFields: Map<String, Field> by lazy { collectSavableFields() }

    fun saveInstanceState(data: Bundle?, prefix: String? = null) {
        data ?: return
        savableFields.forEach { (name, field) -> saveField(data, prefix, name, field) }
    }

    @Suppress("UNCHECKED_CAST")
    private fun saveField(data: Bundle, prefix: String?, fieldName: String, field: Field) {
        val fieldNameWithPrefix = (prefix?.let { "$it." } ?: "") + fieldName
        val value = field.get(this)

        if (value is Savable) {
            value.saveInstanceState(data, fieldNameWithPrefix)
            return
        }

        when (value) {
            is Boolean -> data.putBoolean(fieldNameWithPrefix, value)
            is Byte -> data.putByte(fieldNameWithPrefix, value)
            is Int -> data.putInt(fieldNameWithPrefix, value)
            is Char -> data.putChar(fieldNameWithPrefix, value)
            is Short -> data.putShort(fieldNameWithPrefix, value)
            is Float -> data.putFloat(fieldNameWithPrefix, value)
            is CharSequence -> data.putCharSequence(fieldNameWithPrefix, value)
            is Parcelable -> data.putParcelable(fieldNameWithPrefix, value)
            is String -> data.putString(fieldNameWithPrefix, value)
            is Serializable -> data.putSerializable(fieldNameWithPrefix, value)

            is ByteArray -> data.putByteArray(fieldNameWithPrefix, value)
            is CharArray -> data.putCharArray(fieldNameWithPrefix, value)
            is IntArray -> data.putIntArray(fieldNameWithPrefix, value)
            is FloatArray -> data.putFloatArray(fieldNameWithPrefix, value)
            is ShortArray -> data.putShortArray(fieldNameWithPrefix, value)
            is DoubleArray -> data.putDoubleArray(fieldNameWithPrefix, value)
            is LongArray -> data.putLongArray(fieldNameWithPrefix, value)

            is Array<*> ->
                when (field.type.componentType) {
                    Parcelable::class.java -> data.putParcelableArray(fieldNameWithPrefix, value as Array<Parcelable>)
                    String::class.java -> data.putStringArray(fieldNameWithPrefix, value as Array<String>)
                    CharSequence::class.java -> data.putCharSequenceArray(fieldNameWithPrefix, value as Array<CharSequence>)
                    else -> throwNotSupportedTypeException(field)
                }

            is ArrayList<*> ->
                when ((field.genericType as ParameterizedType).actualTypeArguments[0]) {
                    Parcelable::class.java -> data.putParcelableArrayList(fieldNameWithPrefix, value as ArrayList<Parcelable>)
                    String::class.java -> data.putStringArrayList(fieldNameWithPrefix, value as ArrayList<String>)
                    Int::class.java -> data.putIntegerArrayList(fieldNameWithPrefix, value as ArrayList<Int>)
                    CharSequence::class.java -> data.putCharSequenceArrayList(fieldNameWithPrefix, value as ArrayList<CharSequence>)
                    else -> throwNotSupportedTypeException(field)
                }


            is SparseArray<*> ->
                when ((field.genericType as ParameterizedType).actualTypeArguments[0]) {
                    Parcelable::class.java -> data.putSparseParcelableArray(fieldNameWithPrefix, value as SparseArray<Parcelable>)
                    else -> throwNotSupportedTypeException(field)
                }


            else -> throwNotSupportedTypeException(field)
        }
    }

    private fun restoreInstanceState(data: Bundle?, prefix: String? = null) {
        data ?: return
        savableFields.forEach { (name, field) -> restoreField(data, prefix, name, field) }
    }

    private fun restoreField(data: Bundle, prefix: String?, fieldName: String, field: Field) {
        val fieldNameWithPrefix = (prefix?.let { "$it." } ?: "") + fieldName
        val value = readValue(data, fieldNameWithPrefix, field) ?: return
        field.set(this@Savable, value)
    }

    @Suppress("UNCHECKED_CAST")
    private fun readValue(data: Bundle, fieldNameWithPrefix: String, field: Field): Any? {

        if (Savable::class.java.isAssignableFrom(field.type)) {
            if (!data.containsKey(fieldNameWithPrefix)) {
                return null
            }
            val innerSavable = createSavableInstance(field.type as Class<out Savable>)
            return innerSavable.restoreInstanceState(data, fieldNameWithPrefix)
        }

        if (Serializable::class.java.isAssignableFrom(field.type)) {
            return data.getSerializable(fieldNameWithPrefix)
        }

        return when (field.type) {
            Boolean::class.java -> data.getBoolean(fieldNameWithPrefix)
            Byte::class.java -> data.getByte(fieldNameWithPrefix)
            Int::class.java -> data.getInt(fieldNameWithPrefix)
            Char::class.java -> data.getChar(fieldNameWithPrefix)
            Short::class.java -> data.getShort(fieldNameWithPrefix)
            Float::class.java -> data.getFloat(fieldNameWithPrefix)
            CharSequence::class.java -> data.getCharSequence(fieldNameWithPrefix)
            Parcelable::class.java -> data.getParcelable(fieldNameWithPrefix)
            String::class.java -> data.getString(fieldNameWithPrefix)

            ByteArray::class.java -> data.getByteArray(fieldNameWithPrefix)
            CharArray::class.java -> data.getCharArray(fieldNameWithPrefix)
            IntArray::class.java -> data.getIntArray(fieldNameWithPrefix)
            FloatArray::class.java -> data.getFloatArray(fieldNameWithPrefix)
            ShortArray::class.java -> data.getShortArray(fieldNameWithPrefix)
            DoubleArray::class.java -> data.getDoubleArray(fieldNameWithPrefix)
            LongArray::class.java -> data.getLongArray(fieldNameWithPrefix)

            Array<String>::class.java -> data.getStringArray(fieldNameWithPrefix)
            Array<CharSequence>::class.java -> data.getCharSequenceArray(fieldNameWithPrefix)
            Array<Parcelable>::class.java -> data.getParcelableArray(fieldNameWithPrefix)

            ArrayList::class.java ->
                when ((field.genericType as ParameterizedType).actualTypeArguments[0]) {
                    Parcelable::class.java -> data.getParcelableArrayList(fieldNameWithPrefix)
                    String::class.java -> data.getStringArrayList(fieldNameWithPrefix)
                    Int::class.java -> data.getIntegerArrayList(fieldNameWithPrefix)
                    CharSequence::class.java -> data.getCharSequenceArrayList(fieldNameWithPrefix)
                    else -> throwNotSupportedTypeException(field)
                }


            SparseArray::class.java ->
                when ((field.genericType as ParameterizedType).actualTypeArguments[0]) {
                    Parcelable::class.java -> data.getSparseParcelableArray<Parcelable>(fieldNameWithPrefix)
                    else -> throwNotSupportedTypeException(field)
                }


            else -> throwNotSupportedTypeException(field)
        }
    }

    private fun throwNotSupportedTypeException(field: Field): Nothing =
            throw IllegalArgumentException("Type ${field.type.simpleName} of field ${field.name} of class ${this@Savable::class.java.simpleName} not supported to saving to Bundle")

    private fun collectSavableFields() =
            this@Savable::class.java.declaredFields
                    .mapNotNull { field ->
                        field.annotations.forEach {
                            if (it.annotationClass == UnsavableField::class) {
                                return@mapNotNull null
                            }
                        }
                        field.isAccessible = true
                        return@mapNotNull (field.name) to field
                    }
                    .associate { it }
}