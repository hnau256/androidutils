package ru.hnau.androidutils.utils.handler


import ru.hnau.jutils.TimeValue


abstract class ContainerWithDeferredClearing<T : Any>(
        private val pauseBeforeClear: TimeValue
) {

    class Connection<T : Any>(
            val value: T,
            private val container: ContainerWithDeferredClearing<T>
    ) {
        fun disconnect() = container.disconnect(this)
    }

    private val needClearRemoveHandlerWaiter = HandlerWaiter {
        synchronized(this) {
            val valueLocal = this.value ?: return@synchronized
            destroyValue(valueLocal)
            value = null
        }
    }

    private var value: T? = null

    private val connections = ArrayList<Connection<T>>()

    fun get() = connect().let { connection ->
        val value = connection.value
        connection.disconnect()
        return@let value
    }

    fun connect() = synchronized(this) {
        val connection = Connection(getOrCreateValueForFirstConnection(), this)
        connections.add(connection)
        needClearRemoveHandlerWaiter.cancel()
        return@synchronized connection
    }

    fun disconnect(connection: Connection<T>) = synchronized(this) {
        if (!connections.remove(connection)) {
            return@synchronized
        }

        if (connections.isEmpty()) {
            needClearRemoveHandlerWaiter.start(pauseBeforeClear)
        }
    }

    private fun getOrCreateValueForFirstConnection(): T = synchronized(this) {
        val existenceValue = this.value
        if (existenceValue != null) {
            return@synchronized existenceValue
        }
        val newValue = createValue()
        this.value = newValue
        return@synchronized newValue
    }

    protected abstract fun createValue(): T

    protected abstract fun destroyValue(content: T)

}