package ru.hnau.androidutilsapp

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp
import ru.hnau.androidutils.context_getters.dp_px.dp16
import ru.hnau.androidutils.context_getters.dp_px.dp32
import ru.hnau.androidutils.context_getters.dp_px.dp8
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.canvas_shape.RoundSidesRectCanvasShape
import ru.hnau.androidutils.ui.drawer.ripple.RippleDrawer
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.ui.view.utils.touch.TouchHandler


@SuppressLint("ViewConstructor")
class AppButton(
        context: Context,
        onClick: () -> Unit,
        initialText: StringGetter = StringGetter.EMPTY
) : Label(
        context = context,
        initialText = initialText,
        textColor = ColorManager.BACKGROUND,
        minLines = 1,
        maxLines = 1,
        textSize = dp(20),
        gravity = HGravity.CENTER
) {

    private val boundsProducer = ViewBoundsProducer(
            view = this,
            usePaddings = false
    )

    private val canvasShape = RoundSidesRectCanvasShape(boundsProducer)

    private val touchHandler = TouchHandler(
            canvasShape = canvasShape,
            onClicked = onClick
    )

    private val rippleDrawer = RippleDrawer(
            animatingView = this,
            touchHandler = touchHandler,
            canvasShape = canvasShape,
            rippleDrawInfo = ColorManager.FG_RIPPLE_DRAW_INFO
    )

    init {
        setPadding(dp32, dp16)
        setMarginParams(MATCH_PARENT, WRAP_CONTENT) {
            setMargins(dp16.getPx(context), dp8.getPx(context))
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        super.onTouchEvent(event)
        touchHandler.handle(event)
        return true
    }

    override fun draw(canvas: Canvas) {
        rippleDrawer.draw(canvas)
        super.draw(canvas)
    }

}


fun appButton(
        text: StringGetter = StringGetter(),
        onClick: (() -> Unit),
        viewConfigurator: (AppButton.() -> Unit)? = null
) = ViewGetter(
        viewCreator = { AppButton(it, onClick, text) },
        viewConfigurator = viewConfigurator
)

fun ViewGroup.addAppButton(
        text: StringGetter = StringGetter(),
        onClick: (() -> Unit),
        viewConfigurator: (AppButton.() -> Unit)? = null
) =
        addView(
                AppButton(context, onClick, text),
                viewConfigurator
        )