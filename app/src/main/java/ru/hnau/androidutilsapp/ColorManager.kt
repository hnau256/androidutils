package ru.hnau.androidutilsapp

import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.ui.drawer.ripple.info.RippleDrawInfo


object ColorManager {

    val BLUE = ColorGetter.byResId(R.color.blue)
    val RED = ColorGetter.byResId(R.color.red)

    val BACKGROUND = ColorGetter.choose { AppTheme.current.background }

    val FOREGROUND = ColorGetter.choose { AppTheme.current.foreground }

    val BG_RIPPLE_DRAW_INFO = RippleDrawInfo(
            backgroundColor = BACKGROUND,
            color = FOREGROUND
    )

    val FG_RIPPLE_DRAW_INFO = RippleDrawInfo(
            backgroundColor = FOREGROUND,
            color = BACKGROUND
    )

}