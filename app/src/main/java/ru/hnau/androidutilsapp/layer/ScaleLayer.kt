package ru.hnau.androidutilsapp.layer

import android.content.Context
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.ScaleManager
import ru.hnau.androidutilsapp.layer.base.ListLayer


class ScaleLayer(context: Context) : ListLayer(
        context = context,
        title = "Scale".toGetter(),
        items = listOf(
                0.125f, 0.25f, 0.4f, 0.5f, 0.75f, 0.9f, 1f, 1.125f, 1.25f, 1.5f, 1.75f, 2f
        ).map { scale ->
            ListLayer.Item(
                    title = "x$scale".toGetter(),
                    onClick = { ScaleManager.scaleFactor = scale }
            )
        }
)